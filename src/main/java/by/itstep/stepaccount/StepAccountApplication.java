package by.itstep.stepaccount;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StepAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(StepAccountApplication.class, args);
    }
}
