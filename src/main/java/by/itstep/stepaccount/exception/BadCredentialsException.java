package by.itstep.stepaccount.exception;

public class BadCredentialsException extends RuntimeException{

    private static final String WRONG_EMAIL_OR_PASSWORD = "Wrong email or password";

    public BadCredentialsException() {
        super(WRONG_EMAIL_OR_PASSWORD);
    }
}
