package by.itstep.stepaccount.exception;

public class EntityIsNotFoundException extends RuntimeException {

    public EntityIsNotFoundException(String message) {
        super(message);
    }
}
