package by.itstep.stepaccount.exception;

public class GroupNameIsTakenException extends RuntimeException {

    public GroupNameIsTakenException(final String message) {
        super(message);
    }
}
