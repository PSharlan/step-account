package by.itstep.stepaccount.exception;

public class UserCredentialsAreTakenException extends RuntimeException {

    public UserCredentialsAreTakenException(String message) {
        super(message);
    }
}
