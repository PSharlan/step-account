package by.itstep.stepaccount.exception;

public class NotMatchingTheRoleForTheUserException extends RuntimeException {

    public NotMatchingTheRoleForTheUserException(final String message) {
        super(message);
    }
}
