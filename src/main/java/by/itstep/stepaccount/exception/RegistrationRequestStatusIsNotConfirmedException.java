package by.itstep.stepaccount.exception;

public class RegistrationRequestStatusIsNotConfirmedException extends RuntimeException {

    public RegistrationRequestStatusIsNotConfirmedException(final String message) {
        super(message);
    }
}
