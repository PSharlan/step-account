package by.itstep.stepaccount.dto.group;

import by.itstep.stepaccount.dto.student.StudentPreviewDto;
import by.itstep.stepaccount.dto.teacher.TeacherPreviewDto;
import by.itstep.stepaccount.entity.enums.Subject;
import by.itstep.stepaccount.entity.enums.TrainingPeriod;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class GroupFullDto {

    private Integer id;
    private Subject subjectOfStudy;
    private String name;
    private Instant startDate;
    private TrainingPeriod trainingPeriod;
    private List<StudentPreviewDto> students;
    private TeacherPreviewDto teacher;
}
