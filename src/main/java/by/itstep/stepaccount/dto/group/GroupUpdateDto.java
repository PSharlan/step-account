package by.itstep.stepaccount.dto.group;

import by.itstep.stepaccount.entity.enums.Subject;
import by.itstep.stepaccount.entity.enums.TrainingPeriod;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.Instant;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class GroupUpdateDto {

    @ApiModelProperty(example = "1",notes = "Id of the current group")
    @NotNull(message = "id can not be null")
    private Integer id;

    @ApiModelProperty(example = "J2021",notes = "Must be unique")
    @NotEmpty(message = "name can not be empty")
    private String name;

    @ApiModelProperty(example = "COMPUTER_SCIENCE",notes = "Must be from existing enumeration")
    @NotNull(message = "subjectOfStudy can not be empty")
    private Subject subjectOfStudy;

    @ApiModelProperty(example = "2021-01-04T21:07:00.290Z",notes = "start date of training")
    @NotNull(message = "startDate can not be empty")
    private Instant startDate;

    @ApiModelProperty(example = "FOR_YEARS",notes = "Must be from existing enumeration")
    @NotNull(message = "trainingPeriod can not be empty")
    private TrainingPeriod trainingPeriod;

    @ApiModelProperty(example = "3",notes = "Link to the image file")
    @NotNull(message = "teacherId can not be null")
    private Integer teacherId;
}
