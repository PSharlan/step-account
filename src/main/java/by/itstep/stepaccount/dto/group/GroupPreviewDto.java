package by.itstep.stepaccount.dto.group;

import by.itstep.stepaccount.dto.teacher.TeacherPreviewDto;
import by.itstep.stepaccount.entity.enums.Subject;
import lombok.Data;

import java.time.Instant;

@Data
public class GroupPreviewDto {

    private Integer id;
    private Subject subjectOfStudy;
    private String name;
    private Instant startDate;
    private TeacherPreviewDto teacher;
}
