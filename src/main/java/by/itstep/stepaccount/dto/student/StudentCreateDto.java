package by.itstep.stepaccount.dto.student;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class StudentCreateDto {

    @ApiModelProperty(example = "Bob")
    @NotEmpty(message = "name can not be empty")
    private String name;

    @ApiModelProperty(example = "Bobson")
    @NotEmpty(message = "lastName can not be empty")
    private String lastName;

    @ApiModelProperty(example = "bob@gmail.com", notes = "Must be unique")
    @NotEmpty(message = "email can not be empty")
    private String email;

    @ApiModelProperty(example = "123asd127K8")
    @NotEmpty(message = "password can not be empty")
    private String password;

    @ApiModelProperty(example = "5.0")
    @NotNull(message = "rating can not be null")
    private Double rating;

    @ApiModelProperty(example = "1", notes = "groupId can not be empty")
    @NotNull(message = "groupId can not be null")
    private Integer groupId;
}
