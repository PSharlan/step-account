package by.itstep.stepaccount.dto.student;

import lombok.Data;

@Data
public class StudentPreviewDto {

    private Integer id;
    private String name;
    private String lastName;
    private String email;
    private Double rating;
}
