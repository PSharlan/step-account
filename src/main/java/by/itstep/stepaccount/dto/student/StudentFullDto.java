package by.itstep.stepaccount.dto.student;

import by.itstep.stepaccount.dto.group.GroupPreviewDto;
import by.itstep.stepaccount.entity.enums.UserRole;
import lombok.Data;

import java.time.Instant;

@Data
public class StudentFullDto {

    private Integer id;
    private String name;
    private String lastName;
    private String email;
    private Instant registrationDate;
    private Instant lastLoginDate;
    private Double rating;
    private GroupPreviewDto group;
    private UserRole userRole;
}
