package by.itstep.stepaccount.dto.admin;

import lombok.Data;

@Data
public class AdminFullDto {

    private Integer id;
    private String login;
}
