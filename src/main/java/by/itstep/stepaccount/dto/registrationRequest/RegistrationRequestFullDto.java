package by.itstep.stepaccount.dto.registrationRequest;

import by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus;
import lombok.Data;

@Data
public class RegistrationRequestFullDto {

    private Integer id;
    private RegistrationRequestEntityStatus status;
    private Integer userId;
    private String userName;
    private String userLastName;
    private String userEmail;
}
