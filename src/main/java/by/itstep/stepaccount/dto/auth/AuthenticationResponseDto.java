package by.itstep.stepaccount.dto.auth;

import by.itstep.stepaccount.entity.enums.UserRole;
import lombok.Data;

@Data
public class AuthenticationResponseDto {

    private Integer id;
    private String email;
    private UserRole userRole;
    private String token;
}
