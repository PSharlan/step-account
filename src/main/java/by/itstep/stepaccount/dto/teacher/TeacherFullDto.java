package by.itstep.stepaccount.dto.teacher;

import by.itstep.stepaccount.dto.group.GroupPreviewDto;
import by.itstep.stepaccount.entity.enums.UserRole;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class TeacherFullDto {

    private Integer id;
    private String name;
    private String lastName;
    private String email;
    private String password;
    private Instant registrationDate;
    private Instant lastLoginDate;
    private UserRole userRole;
    private List<GroupPreviewDto> teachingGroups;
}
