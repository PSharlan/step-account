package by.itstep.stepaccount.dto.teacher;

import lombok.Data;

@Data
public class TeacherPreviewDto {

    private Integer id;
    private String name;
    private String lastName;
    private String email;
}
