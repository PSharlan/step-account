package by.itstep.stepaccount.dto.teacher;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class TeacherCreateDto {

    @ApiModelProperty(example = "Bob")
    @NotEmpty(message = "name can not be empty")
    private String name;

    @ApiModelProperty(example = "Bobson")
    @NotEmpty(message = "lastName can not be empty")
    private String lastName;

    @ApiModelProperty(example = "bob@gmail.com", notes = "Must be unique")
    @NotEmpty(message = "email can not be empty")
    private String email;

    @ApiModelProperty(example = "123asd127K8")
    @NotEmpty(message = "password can not be empty")
    private String password;
}
