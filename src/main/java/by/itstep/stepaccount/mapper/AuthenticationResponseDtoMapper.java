package by.itstep.stepaccount.mapper;

import by.itstep.stepaccount.dto.auth.AuthenticationResponseDto;
import by.itstep.stepaccount.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AuthenticationResponseDtoMapper {

    @Mapping(target = "token", ignore = true)
    AuthenticationResponseDto mapToAuthenticationResponseDto(UserEntity userEntity);
}
