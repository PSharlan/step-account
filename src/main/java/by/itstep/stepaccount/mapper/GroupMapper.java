package by.itstep.stepaccount.mapper;

import by.itstep.stepaccount.dto.group.GroupCreateDto;
import by.itstep.stepaccount.dto.group.GroupFullDto;
import by.itstep.stepaccount.dto.group.GroupPreviewDto;
import by.itstep.stepaccount.dto.group.GroupUpdateDto;
import by.itstep.stepaccount.entity.GroupEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring",
    uses = { TeacherMapper.class, StudentMapper.class })
public interface GroupMapper {

    GroupFullDto mapToFullDto(GroupEntity groupEntity);

    @Mapping(target = "teacher", ignore = true)
    @Mapping(target = "students", ignore = true)
    @Mapping(target = "id", ignore = true)
    GroupEntity mapToEntity(GroupCreateDto createDto);

    GroupPreviewDto mapToPreviewDto(GroupEntity entity);

    List<GroupPreviewDto> mapToPreviewDtoList(List<GroupEntity> entities);

    @Mapping(target = "teacher", ignore = true)
    @Mapping(target = "students", ignore = true)
    void updateEntity(GroupUpdateDto updateDto, @MappingTarget GroupEntity userEntity);

}
