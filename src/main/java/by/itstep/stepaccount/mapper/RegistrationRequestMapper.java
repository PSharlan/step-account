package by.itstep.stepaccount.mapper;

import by.itstep.stepaccount.dto.registrationRequest.RegistrationRequestFullDto;
import by.itstep.stepaccount.entity.RegistrationRequestEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RegistrationRequestMapper {

    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "userLastName", source = "user.lastName")
    @Mapping(target = "userName", source = "user.name")
    @Mapping(target = "userEmail", source = "user.email")
    RegistrationRequestFullDto mapToFullDto(RegistrationRequestEntity registrationRequestEntity);

    List<RegistrationRequestFullDto> mapToFullDtoList(List<RegistrationRequestEntity> registrationRequestEntities);
}
