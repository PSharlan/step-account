package by.itstep.stepaccount.mapper;

import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.dto.student.StudentPreviewDto;
import by.itstep.stepaccount.dto.student.StudentUpdateDto;
import by.itstep.stepaccount.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(
    componentModel = "spring",
    uses = GroupMapper.class
)
public interface StudentMapper {

    StudentFullDto mapToFullDto(UserEntity userEntity);

    @Mapping(target = "userRole", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "teachingGroups", ignore = true)
    @Mapping(target = "registrationRequest", ignore = true)
    @Mapping(target = "lastLoginDate", ignore = true)
    @Mapping(target = "group", ignore = true)
    @Mapping(target = "id", ignore = true)
    UserEntity mapToEntity(StudentCreateDto createDto);

    StudentPreviewDto mapToPreviewDto(UserEntity entity);

    List<StudentPreviewDto> mapToPreviewDtoList(List<UserEntity> entities);

    @Mapping(target = "userRole", ignore = true)
    @Mapping(target = "teachingGroups", ignore = true)
    @Mapping(target = "registrationRequest", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "lastLoginDate", ignore = true)
    @Mapping(target = "email", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "group", ignore = true)
    void updateEntity(StudentUpdateDto updateDto, @MappingTarget UserEntity userEntity);

}
