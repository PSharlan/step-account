package by.itstep.stepaccount.mapper;

import by.itstep.stepaccount.dto.admin.AdminFullDto;
import by.itstep.stepaccount.entity.AdminEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AdminMapper {

    AdminFullDto mapToFullDto(AdminEntity adminEntity);
}
