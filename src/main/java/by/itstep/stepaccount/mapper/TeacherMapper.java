package by.itstep.stepaccount.mapper;

import by.itstep.stepaccount.dto.teacher.TeacherCreateDto;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherPreviewDto;
import by.itstep.stepaccount.dto.teacher.TeacherUpdateDto;
import by.itstep.stepaccount.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(
    componentModel = "spring",
    uses = GroupMapper.class
)
public interface TeacherMapper {

    TeacherFullDto mapToFullDto(UserEntity userEntity);

    @Mapping(target = "userRole", ignore = true)
    @Mapping(target = "teachingGroups", ignore = true)
    @Mapping(target = "registrationRequest", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "lastLoginDate", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "group", ignore = true)
    UserEntity mapToEntity(TeacherCreateDto createDto);

    TeacherPreviewDto mapToPreviewDto(UserEntity entity);

    List<TeacherPreviewDto> mapToPreviewDtoList(List<UserEntity> entities);

    @Mapping(target = "userRole", ignore = true)
    @Mapping(target = "teachingGroups", ignore = true)
    @Mapping(target = "registrationRequest", ignore = true)
    @Mapping(target = "registrationDate", ignore = true)
    @Mapping(target = "lastLoginDate", ignore = true)
    @Mapping(target = "group", ignore = true)
    void updateEntity(TeacherUpdateDto updateDto, @MappingTarget UserEntity userEntity);

}
