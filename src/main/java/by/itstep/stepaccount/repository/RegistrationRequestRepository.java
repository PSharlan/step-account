package by.itstep.stepaccount.repository;

import by.itstep.stepaccount.entity.RegistrationRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RegistrationRequestRepository extends JpaRepository<RegistrationRequestEntity, Integer> {

    RegistrationRequestEntity findByUserId(Integer userId);

    List<RegistrationRequestEntity> findAllByUserGroupId(Integer groupId);
}
