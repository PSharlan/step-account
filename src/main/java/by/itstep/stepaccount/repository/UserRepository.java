package by.itstep.stepaccount.repository;

import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.entity.enums.UserRole;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    Optional<UserEntity> findByEmail(String email);

    Page<UserEntity> findAllByUserRole(UserRole role, Pageable pageable);

    boolean existsByIdAndUserRole(Integer id, UserRole userRole);

    Optional<UserEntity> findByIdAndUserRole(Integer id, UserRole userRole);
}
