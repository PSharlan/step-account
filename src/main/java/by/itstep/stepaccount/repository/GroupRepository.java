package by.itstep.stepaccount.repository;

import by.itstep.stepaccount.entity.GroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface GroupRepository extends JpaRepository<GroupEntity, Integer> {

    Optional<GroupEntity> findByName(String name);

    @Query(nativeQuery = true,value =
            "SELECT * FROM `group` " +
                    "JOIN user u ON u.id = `group`.teacher_id OR `group`.id = u.group_id " +
                    "WHERE u.id = :userId")
    List<GroupEntity> findAllByUserId(@Param("userId") Integer userId);

}
