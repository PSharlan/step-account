package by.itstep.stepaccount.service;

import by.itstep.stepaccount.dto.teacher.TeacherCreateDto;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherPreviewDto;
import by.itstep.stepaccount.dto.teacher.TeacherUpdateDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TeacherService {

    TeacherFullDto findById(Integer id);

    Page<TeacherPreviewDto> findAll(Pageable pageable);

    TeacherFullDto create(TeacherCreateDto createDto);

    TeacherFullDto update(TeacherUpdateDto updateDto);

    void deleteById(Integer id);
}
