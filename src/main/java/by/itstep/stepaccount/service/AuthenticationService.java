package by.itstep.stepaccount.service;

import by.itstep.stepaccount.dto.auth.AuthenticationResponseDto;

public interface AuthenticationService {

    AuthenticationResponseDto login(String email, String password);
}
