package by.itstep.stepaccount.service;

import by.itstep.stepaccount.dto.registrationRequest.RegistrationRequestFullDto;
import by.itstep.stepaccount.entity.UserEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RegistrationRequestService {

    RegistrationRequestFullDto createRequest(UserEntity userEntity);

    RegistrationRequestFullDto confirmRequest(Integer requestId);

    Page<RegistrationRequestFullDto> findAll(Pageable pageable);

    List<RegistrationRequestFullDto> findAllByGroupId(Integer groupId);
}
