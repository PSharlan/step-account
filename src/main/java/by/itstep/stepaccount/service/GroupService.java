package by.itstep.stepaccount.service;

import by.itstep.stepaccount.dto.group.GroupCreateDto;
import by.itstep.stepaccount.dto.group.GroupFullDto;
import by.itstep.stepaccount.dto.group.GroupPreviewDto;
import by.itstep.stepaccount.dto.group.GroupUpdateDto;
import by.itstep.stepaccount.entity.GroupEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface GroupService {

    GroupFullDto findById(Integer id);

    Page<GroupPreviewDto> findAll(Pageable pageable);

    List<GroupPreviewDto> findAllByUserId(Integer userId);

    GroupFullDto create(GroupCreateDto createDto);

    GroupFullDto update(GroupUpdateDto updateDto);

    void deleteById(Integer id);

    GroupFullDto findByName(String name);
}
