package by.itstep.stepaccount.service.impl;

import static org.springframework.util.DigestUtils.md5DigestAsHex;
import by.itstep.stepaccount.dto.auth.AuthenticationResponseDto;
import by.itstep.stepaccount.entity.RegistrationRequestEntity;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus;
import by.itstep.stepaccount.exception.BadCredentialsException;
import by.itstep.stepaccount.exception.RegistrationRequestStatusIsNotConfirmedException;
import by.itstep.stepaccount.mapper.AuthenticationResponseDtoMapper;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    @Value("${authenticationProperties.checkConfirmEnabled}")
    private Boolean checkConfirmEnabled;

    private final UserRepository userRepository;

    private final AuthenticationResponseDtoMapper mapper;

    @Override
    public AuthenticationResponseDto login(String email, String password) {
        AuthenticationResponseDto responseDto = userRepository.findByEmail(email)
            .filter(user -> user.getPassword().equals(password))
            .map(this::generateAuthenticationResponseDto)
            .orElseThrow(BadCredentialsException::new);

        log.info("AuthenticationServiceImpl -> login is ok, by email: {}", email);
        return responseDto;
    }

    private AuthenticationResponseDto generateAuthenticationResponseDto(UserEntity user) {
        if(checkConfirmEnabled){
            checkRegistrationRequest(user.getRegistrationRequest());
        }
        AuthenticationResponseDto dto = mapper.mapToAuthenticationResponseDto(user);
        dto.setToken(md5DigestAsHex(user.getEmail().getBytes()));
        return dto;
    }

    private void checkRegistrationRequest(RegistrationRequestEntity request) {
        Boolean isAccepted = request.getStatus().equals(RegistrationRequestEntityStatus.ACCEPTED);
        if (!isAccepted) {
            throw new RegistrationRequestStatusIsNotConfirmedException(String.format(
                "RegistrationRequest with id: %s -> is not confirmed and has status %s",
                request.getId(), request.getStatus()));
        }
    }
}
