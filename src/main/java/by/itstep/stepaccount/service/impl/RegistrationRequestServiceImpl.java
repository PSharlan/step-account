package by.itstep.stepaccount.service.impl;

import static by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus.ACCEPTED;
import static by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus.PENDING;
import static java.util.stream.Collectors.toList;
import by.itstep.stepaccount.dto.registrationRequest.RegistrationRequestFullDto;
import by.itstep.stepaccount.entity.RegistrationRequestEntity;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.exception.EntityIsNotFoundException;
import by.itstep.stepaccount.mapper.RegistrationRequestMapper;
import by.itstep.stepaccount.repository.RegistrationRequestRepository;
import by.itstep.stepaccount.service.RegistrationRequestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class RegistrationRequestServiceImpl implements RegistrationRequestService {

    private final RegistrationRequestRepository registrationRequestRepository;
    private final RegistrationRequestMapper requestMapper;

    @Override
    public RegistrationRequestFullDto createRequest(UserEntity user) {
        RegistrationRequestEntity requestEntity = new RegistrationRequestEntity();
        requestEntity.setUser(user);
        requestEntity.setStatus(PENDING);

        RegistrationRequestEntity savedRequest = registrationRequestRepository.save(requestEntity);

        log.info("RegistrationRequestServiceImpl -> request {} successfully saved", savedRequest);
        return requestMapper.mapToFullDto(savedRequest);
    }

    @Override
    public RegistrationRequestFullDto confirmRequest(Integer requestId) {
        RegistrationRequestEntity foundRequest = registrationRequestRepository.findById(requestId)
            .orElseThrow(() -> new EntityIsNotFoundException(
                String.format("RegistrationRequestServiceImpl -> request was not found by id: %s", requestId)));

        foundRequest.setStatus(ACCEPTED);
        RegistrationRequestEntity updatedRequest = registrationRequestRepository.save(foundRequest);

        log.info("RegistrationRequestServiceImpl -> request {} successfully updated", updatedRequest);
        return requestMapper.mapToFullDto(updatedRequest);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RegistrationRequestFullDto> findAll(Pageable pageable) {
        Page<RegistrationRequestEntity> page = registrationRequestRepository.findAll(pageable);
        return page.map(requestMapper::mapToFullDto);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RegistrationRequestFullDto> findAllByGroupId(Integer groupId) {
        return registrationRequestRepository.findAllByUserGroupId(groupId)
            .stream()
            .map(requestMapper::mapToFullDto)
            .collect(toList());
    }
}
