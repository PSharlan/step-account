package by.itstep.stepaccount.service.impl;

import by.itstep.stepaccount.dto.admin.AdminFullDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.exception.EntityIsNotFoundException;
import by.itstep.stepaccount.exception.UserCredentialsAreTakenException;
import by.itstep.stepaccount.mapper.AdminMapper;
import by.itstep.stepaccount.repository.AdminRepository;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.AdminService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

    private final AdminRepository adminRepository;

    private final AdminMapper adminMapper;

    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public AdminFullDto findById(Integer id) {
        AdminFullDto adminFullDto = adminRepository.findById(id)
            .map(adminMapper::mapToFullDto)
            .orElseThrow(() -> new EntityIsNotFoundException(String.format("Admin was not found by id: %s", id)));

        log.info("AdminServiceImpl ->  found admin: {}", adminFullDto);

        return adminFullDto;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        checkAdminForExist(id);
        adminRepository.deleteById(id);

        log.info("AdminServiceImpl -> admin by id: {} successfully deleted", id);
    }

    @Override
    @Transactional
    public void checkIfEmailTaken(UserEntity userEntity) {
        userRepository.findByEmail(userEntity.getEmail())
            .filter(user -> !user.getId().equals(userEntity.getId()))
            .ifPresent(user -> {
                throw new UserCredentialsAreTakenException(
                    String.format("Email exists: %s", user.getEmail()));
            });
    }

    private void checkAdminForExist(final Integer id) {
        if (!adminRepository.existsById(id)) {
            throw new EntityIsNotFoundException(String.format("Admin was not found by id: %s", id));
        }
    }
}
