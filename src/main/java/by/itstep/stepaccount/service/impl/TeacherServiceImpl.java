package by.itstep.stepaccount.service.impl;

import static by.itstep.stepaccount.entity.enums.UserRole.TEACHER;
import by.itstep.stepaccount.dto.teacher.TeacherCreateDto;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherPreviewDto;
import by.itstep.stepaccount.dto.teacher.TeacherUpdateDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.exception.EntityIsNotFoundException;
import by.itstep.stepaccount.mapper.TeacherMapper;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.AdminService;
import by.itstep.stepaccount.service.RegistrationRequestService;
import by.itstep.stepaccount.service.TeacherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Slf4j
@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {

    private final UserRepository userRepository;

    private final RegistrationRequestService requestService;

    private final TeacherMapper teacherMapper;

    private final AdminService adminService;

    @Override
    @Transactional(readOnly = true)
    public TeacherFullDto findById(Integer id) {
        TeacherFullDto teacherFullDto = userRepository.findByIdAndUserRole(id, TEACHER)
            .map(teacherMapper::mapToFullDto)
            .orElseThrow(() -> new EntityIsNotFoundException(String.format("Teacher was not found by id: %s", id)));

        log.info("TeacherServiceImpl ->  found teacher: {}", teacherFullDto);
        return teacherFullDto;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TeacherPreviewDto> findAll(Pageable pageable) {
        Page<UserEntity> page = userRepository.findAllByUserRole(TEACHER, pageable);
        return page.map(teacherMapper::mapToPreviewDto);
    }

    @Override
    @Transactional
    public TeacherFullDto create(TeacherCreateDto teacherCreateDto) {
        UserEntity teacherToSave = teacherMapper.mapToEntity(teacherCreateDto);

        adminService.checkIfEmailTaken(teacherToSave);

        teacherToSave.setUserRole(TEACHER);
        teacherToSave.setRegistrationDate(Instant.now());

        UserEntity savedTeacher = userRepository.save(teacherToSave);

        requestService.createRequest(savedTeacher);

        log.info("TeacherServiceImpl -> teacher {} successfully saved", savedTeacher);
        return teacherMapper.mapToFullDto(savedTeacher);
    }

    @Override
    @Transactional
    public TeacherFullDto update(TeacherUpdateDto teacherUpdateDto) {
        UserEntity teacherToUpdate = userRepository.findByIdAndUserRole(teacherUpdateDto.getId(), TEACHER)
            .orElseThrow(() -> new EntityIsNotFoundException(String.format("Teacher was not found by id: %s", teacherUpdateDto.getId())));

        teacherMapper.updateEntity(teacherUpdateDto, teacherToUpdate);

        adminService.checkIfEmailTaken(teacherToUpdate);

        UserEntity updatedUserEntity = userRepository.save(teacherToUpdate);

        log.info("TeacherServiceImpl -> teacher {} was successfully updated", updatedUserEntity);
        return teacherMapper.mapToFullDto(updatedUserEntity);
    }

    @Override
    public void deleteById(Integer id) {
        checkTeacherForExist(id);
        userRepository.deleteById(id);

        log.info("TeacherServiceImpl -> teacher by id: {} successfully deleted", id);
    }

    private void checkTeacherForExist(final Integer id) {
        if (!userRepository.existsByIdAndUserRole(id, TEACHER)) {
            throw new EntityIsNotFoundException(String.format("Teacher was not found by id: %s", id));
        }
    }
}
