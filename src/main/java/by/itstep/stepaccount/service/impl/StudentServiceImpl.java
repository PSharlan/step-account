package by.itstep.stepaccount.service.impl;

import static by.itstep.stepaccount.entity.enums.UserRole.STUDENT;
import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.dto.student.StudentPreviewDto;
import by.itstep.stepaccount.dto.student.StudentUpdateDto;
import by.itstep.stepaccount.entity.GroupEntity;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.exception.EntityIsNotFoundException;
import by.itstep.stepaccount.mapper.StudentMapper;
import by.itstep.stepaccount.repository.GroupRepository;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.AdminService;
import by.itstep.stepaccount.service.RegistrationRequestService;
import by.itstep.stepaccount.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final UserRepository userRepository;

    private final GroupRepository groupRepository;

    private final RegistrationRequestService requestService;

    private final StudentMapper studentMapper;

    private final AdminService adminService;

    @Override
    @Transactional(readOnly = true)
    public StudentFullDto findById(Integer id) {
        StudentFullDto studentFullDto = userRepository.findByIdAndUserRole(id, STUDENT)
            .map(studentMapper::mapToFullDto)
            .orElseThrow(() -> new EntityIsNotFoundException(String.format("Student was not found by id: %s", id)));

        log.info("StudentServiceImpl ->  found student: {}", studentFullDto);
        return studentFullDto;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<StudentPreviewDto> findAll(Pageable pageable) {
        Page<UserEntity> page = userRepository.findAllByUserRole(STUDENT, pageable);
        return page.map(studentMapper::mapToPreviewDto);
    }

    @Override
    @Transactional
    public StudentFullDto create(StudentCreateDto studentCreateDto) {
        UserEntity studentToSave = studentMapper.mapToEntity(studentCreateDto);

        adminService.checkIfEmailTaken(studentToSave);

        studentToSave.setUserRole(STUDENT);
        studentToSave.setRegistrationDate(Instant.now());

        GroupEntity groupEntity = findGroup(studentCreateDto.getGroupId());
        studentToSave.setGroup(groupEntity);

        UserEntity savedStudent = userRepository.save(studentToSave);

        requestService.createRequest(savedStudent);

        log.info("StudentServiceImpl -> student {} successfully saved", savedStudent);
        return studentMapper.mapToFullDto(savedStudent);
    }

    @Override
    @Transactional
    public StudentFullDto update(StudentUpdateDto studentUpdateDto) {
        UserEntity studentToUpdate = userRepository.findByIdAndUserRole(studentUpdateDto.getId(), STUDENT)
            .orElseThrow(() -> new EntityIsNotFoundException(String.format("Student was not found by id: %s", studentUpdateDto.getId())));

        studentMapper.updateEntity(studentUpdateDto, studentToUpdate);

        GroupEntity group = findGroup(studentUpdateDto.getGroupId());
        studentToUpdate.setGroup(group);

        UserEntity updatedUserEntity = userRepository.save(studentToUpdate);

        log.info("StudentServiceImpl -> student {} was successfully updated", updatedUserEntity);
        return studentMapper.mapToFullDto(updatedUserEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        checkStudentForExist(id);
        userRepository.deleteById(id);

        log.info("StudentServiceImpl -> student by id: {} successfully deleted", id);
    }

    private GroupEntity findGroup(Integer id) {
        return groupRepository.findById(id)
            .orElseThrow(() -> new EntityIsNotFoundException(String.format("Group not found by id: %s", id)));
    }

    private void checkStudentForExist(Integer id) {
        if (!userRepository.existsByIdAndUserRole(id, STUDENT)) {
            throw new EntityIsNotFoundException(String.format("Student was not found by id: %s", id));
        }
    }
}
