package by.itstep.stepaccount.service.impl;

import static by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus.ACCEPTED;
import static java.util.stream.Collectors.*;
import static java.util.stream.Collectors.toList;
import by.itstep.stepaccount.dto.group.GroupCreateDto;
import by.itstep.stepaccount.dto.group.GroupFullDto;
import by.itstep.stepaccount.dto.group.GroupPreviewDto;
import by.itstep.stepaccount.dto.group.GroupUpdateDto;
import by.itstep.stepaccount.dto.registrationRequest.RegistrationRequestFullDto;
import by.itstep.stepaccount.dto.student.StudentPreviewDto;
import by.itstep.stepaccount.entity.GroupEntity;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus;
import by.itstep.stepaccount.exception.EntityIsNotFoundException;
import by.itstep.stepaccount.exception.GroupNameIsTakenException;
import by.itstep.stepaccount.mapper.GroupMapper;
import by.itstep.stepaccount.repository.GroupRepository;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.GroupService;
import by.itstep.stepaccount.service.RegistrationRequestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final GroupRepository groupRepository;

    private final UserRepository userRepository;

    private final GroupMapper groupMapper;

    private final RegistrationRequestService requestService;

    @Override
    @Transactional(readOnly = true)
    public GroupFullDto findById(Integer id) {
        GroupFullDto foundGroup = groupRepository.findById(id)
            .map(groupMapper::mapToFullDto)
            .orElseThrow(() -> new EntityIsNotFoundException(String.format("Group was not found by id: %s", id)));

        log.info("GroupServiceImpl -> found group: {} by id: {}", foundGroup, id);
        return showOnlyAcceptedStudents(foundGroup);
    }

    @Override
    public Page<GroupPreviewDto> findAll(Pageable pageable) {
        Page<GroupEntity> page = groupRepository.findAll(pageable);
        return page.map(groupMapper::mapToPreviewDto);
    }

    @Override
    public List<GroupPreviewDto> findAllByUserId(Integer userId) {
        List<GroupPreviewDto> foundGroups = groupRepository.findAllByUserId(userId)
            .stream()
            .map(groupMapper::mapToPreviewDto)
            .collect(toList());

        log.info("GroupServiceImpl -> found {} groups by user id: {}", foundGroups.size(), userId);
        return foundGroups;
    }

    @Override
    @Transactional
    public GroupFullDto create(GroupCreateDto groupCreateDto) {
        GroupEntity groupEntityToSave = groupMapper.mapToEntity(groupCreateDto);

        checkIfNameTaken(groupEntityToSave);

        GroupEntity savedEntity = groupRepository.save(groupEntityToSave);

        log.info("GroupServiceImpl -> group {} successfully saved", savedEntity);
        return groupMapper.mapToFullDto(savedEntity);
    }

    @Override
    @Transactional
    public GroupFullDto update(GroupUpdateDto groupUpdateDto) {
        GroupEntity groupEntityToUpdate = groupRepository.findById(groupUpdateDto.getId())
            .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                "GroupServiceImpl -> Group was not found by id: %s", groupUpdateDto.getId())));

        groupMapper.updateEntity(groupUpdateDto, groupEntityToUpdate);

        checkIfNameTaken(groupEntityToUpdate);

        UserEntity teacher = findTeacher(groupUpdateDto);
        groupEntityToUpdate.setTeacher(teacher);

        GroupEntity updatedGroupEntity = groupRepository.save(groupEntityToUpdate);

        log.info("GroupServiceImpl -> group {} was successfully updated", updatedGroupEntity);
        return groupMapper.mapToFullDto(updatedGroupEntity);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        checkGroupForExist(id);
        groupRepository.deleteById(id);

        log.info("GroupServiceImpl -> group by id: {} successfully deleted", id);
    }

    @Override
    @Transactional(readOnly = true)
    public GroupFullDto findByName(String name) {
        GroupFullDto foundGroup = groupRepository.findByName(name)
            .map(groupMapper::mapToFullDto)
            .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                "Group was not found by name: %s", name)));

        log.info("GroupServiceImpl -> found group: {} by name: {}", foundGroup, name);
        return showOnlyAcceptedStudents(foundGroup);
    }

    private void checkIfNameTaken(GroupEntity groupEntity) {
        groupRepository.findByName(groupEntity.getName())
            .filter(group -> !group.getId().equals(groupEntity.getId()))
            .ifPresent(group -> {
                throw new GroupNameIsTakenException(String.format(
                    "Group name exists: %s", groupEntity.getName()));
            });
    }

    private UserEntity findTeacher(GroupUpdateDto groupUpdateDto) {
        return userRepository.findById(groupUpdateDto.getTeacherId())
            .orElseThrow(() -> new EntityIsNotFoundException(String.format(
                "Teacher was not found by id: %s", groupUpdateDto.getTeacherId())));
    }

    private void checkGroupForExist(Integer id) {
        if (!groupRepository.existsById(id)) {
            throw new EntityIsNotFoundException(String.format("Group was not found by id: %s", id));
        }
    }

    private GroupFullDto showOnlyAcceptedStudents(GroupFullDto groupFullDto) {
        List<Integer> acceptedUserIds = requestService.findAllByGroupId(groupFullDto.getId()).stream()
                .filter(request -> request.getStatus().equals(ACCEPTED))
                .map(RegistrationRequestFullDto::getUserId)
                .collect(toList());

        List<StudentPreviewDto> onlyAcceptedStudents = groupFullDto.getStudents().stream()
                .filter(student -> acceptedUserIds.contains(student.getId()))
                .collect(toList());

        groupFullDto.setStudents(onlyAcceptedStudents);

        return groupFullDto;
    }
}
