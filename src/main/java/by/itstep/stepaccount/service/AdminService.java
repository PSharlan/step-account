package by.itstep.stepaccount.service;

import by.itstep.stepaccount.dto.admin.AdminFullDto;
import by.itstep.stepaccount.entity.UserEntity;

public interface AdminService {

    AdminFullDto findById(Integer id);

    void deleteById(Integer id);

    void checkIfEmailTaken(UserEntity userEntity);
}
