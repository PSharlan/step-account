package by.itstep.stepaccount.service;

import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.dto.student.StudentPreviewDto;
import by.itstep.stepaccount.dto.student.StudentUpdateDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StudentService {

    StudentFullDto findById(Integer id);

    Page<StudentPreviewDto> findAll(Pageable pageable);

    StudentFullDto create(StudentCreateDto createDto);

    StudentFullDto update(StudentUpdateDto updateDto);

    void deleteById(Integer id);
}
