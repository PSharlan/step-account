package by.itstep.stepaccount.controller;

import by.itstep.stepaccount.dto.group.GroupCreateDto;
import by.itstep.stepaccount.dto.group.GroupFullDto;
import by.itstep.stepaccount.dto.group.GroupPreviewDto;
import by.itstep.stepaccount.dto.group.GroupUpdateDto;
import by.itstep.stepaccount.entity.GroupEntity;
import by.itstep.stepaccount.repository.GroupRepository;
import by.itstep.stepaccount.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/groups")
@RequiredArgsConstructor
public class GroupController {

    private final GroupService groupService;

    @GetMapping("/{id}")
    public GroupFullDto findById(@PathVariable Integer id) {
        return groupService.findById(id);
    }

    @GetMapping
    public Page<GroupPreviewDto> findAll(@PageableDefault(size = Integer.MAX_VALUE) Pageable pageable) {
        return groupService.findAll(pageable);
    }

    @GetMapping("/users/{userId}")
    public List<GroupPreviewDto> findAllByUserId(@PathVariable Integer userId) {
        return groupService.findAllByUserId(userId);
    }

    @PostMapping
    public GroupFullDto create(@Valid @RequestBody GroupCreateDto groupCreateDto) {
        return groupService.create(groupCreateDto);
    }

    @PutMapping
    public GroupFullDto update(@Valid @RequestBody GroupUpdateDto groupUpdateDto) {
        return groupService.update(groupUpdateDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        groupService.deleteById(id);
    }

    @GetMapping("/name/{groupName}")
    public GroupFullDto findByName(@PathVariable String groupName) {
        return groupService.findByName(groupName);
    }
}
