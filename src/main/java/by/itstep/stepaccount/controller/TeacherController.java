package by.itstep.stepaccount.controller;

import by.itstep.stepaccount.dto.teacher.TeacherCreateDto;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherPreviewDto;
import by.itstep.stepaccount.dto.teacher.TeacherUpdateDto;
import by.itstep.stepaccount.service.TeacherService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/teachers")
@RequiredArgsConstructor
public class TeacherController {

    private final TeacherService teacherService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one teacher by id", notes = "Existing id must be specified")
    public TeacherFullDto findById(@PathVariable Integer id) {
        return teacherService.findById(id);
    }

    @GetMapping
    public Page<TeacherPreviewDto> findAll(@PageableDefault(size = Integer.MAX_VALUE) Pageable pageable) {
        return teacherService.findAll(pageable);
    }

    @PostMapping
    @ApiOperation(value = "Create teacher", notes = "When a teacher is created, a registration request is automatically created and linked")
    public TeacherFullDto create(@Valid @RequestBody TeacherCreateDto teacherCreateDto) {
        return teacherService.create(teacherCreateDto);
    }

    @PutMapping
    public TeacherFullDto update(@Valid @RequestBody TeacherUpdateDto teacherUpdateDto) {
        return teacherService.update(teacherUpdateDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        teacherService.deleteById(id);
    }
}
