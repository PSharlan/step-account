package by.itstep.stepaccount.controller;

import by.itstep.stepaccount.dto.auth.AuthenticationResponseDto;
import by.itstep.stepaccount.dto.auth.UserLoginDto;
import by.itstep.stepaccount.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public AuthenticationResponseDto login(@Valid @RequestBody UserLoginDto loginDto) {
        return authenticationService.login(loginDto.getEmail(), loginDto.getPassword());
    }
}
