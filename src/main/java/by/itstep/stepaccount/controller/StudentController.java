package by.itstep.stepaccount.controller;

import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.dto.student.StudentPreviewDto;
import by.itstep.stepaccount.dto.student.StudentUpdateDto;
import by.itstep.stepaccount.service.StudentService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/{id}")
    @ApiOperation(value = "Find one student by id", notes = "Existing id must be specified")
    public StudentFullDto findById(@PathVariable Integer id) {
        return studentService.findById(id);
    }

    @GetMapping
    public Page<StudentPreviewDto> findAll(@PageableDefault(size = Integer.MAX_VALUE) Pageable pageable) {
        return studentService.findAll(pageable);
    }

    @PostMapping
    @ApiOperation(value = "Create student", notes = "When a student is created, a registration request is automatically created and linked")
    public StudentFullDto create(@Valid @RequestBody StudentCreateDto studentCreateDto) {
        return studentService.create(studentCreateDto);
    }

    @PutMapping
    public StudentFullDto update(@Valid @RequestBody StudentUpdateDto studentUpdateDto) {
        return studentService.update(studentUpdateDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        studentService.deleteById(id);
    }
}
