package by.itstep.stepaccount.controller;

import by.itstep.stepaccount.dto.registrationRequest.RegistrationRequestFullDto;
import by.itstep.stepaccount.service.RegistrationRequestService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/registration/requests")
@Api(description = "Controller dedicated to manage registration requests")
@RequiredArgsConstructor
public class RegistrationRequestController {

    private final RegistrationRequestService requestService;

    @PutMapping("/{id}/confirm")
    @ApiOperation(value = "Confirm request", notes = "Change request status - to ACCEPTED")
    public RegistrationRequestFullDto confirm(@PathVariable Integer id) {
        return requestService.confirmRequest(id);
    }

    @GetMapping
    public Page<RegistrationRequestFullDto> findAll(@PageableDefault(size = Integer.MAX_VALUE) Pageable pageable) {
        return requestService.findAll(pageable);
    }

    @GetMapping("/group/{groupId}")
    @ApiOperation(value = "Find all request by groupId")
    public List<RegistrationRequestFullDto> findAllByGroupId(@PathVariable Integer groupId) {
        return requestService.findAllByGroupId(groupId);
    }
}
