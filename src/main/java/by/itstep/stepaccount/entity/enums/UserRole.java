package by.itstep.stepaccount.entity.enums;

public enum UserRole {
    STUDENT,
    TEACHER
}
