package by.itstep.stepaccount.entity.enums;

public enum TrainingPeriod {

    ONE_YEARS,
    TWO_YEARS,
    THREE_YEARS,
    FOR_YEARS
}
