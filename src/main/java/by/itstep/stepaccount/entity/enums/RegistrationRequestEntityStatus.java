package by.itstep.stepaccount.entity.enums;

public enum RegistrationRequestEntityStatus {
    PENDING,
    ACCEPTED
}
