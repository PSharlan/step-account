package by.itstep.stepaccount.entity.enums;

public enum Subject {

    HISTORY,
    MATHEMATICS,
    GEOGRAPHY,
    COMPUTER_SCIENCE
}
