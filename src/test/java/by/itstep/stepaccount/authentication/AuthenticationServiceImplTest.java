package by.itstep.stepaccount.authentication;

import static by.itstep.stepaccount.entity.enums.UserRole.STUDENT;
import static by.itstep.stepaccount.util.DtoGenerationUtils.createUserLoginDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.createUserLoginDtoWithBadCredentials;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import by.itstep.stepaccount.dto.auth.AuthenticationResponseDto;
import by.itstep.stepaccount.dto.auth.UserLoginDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.exception.BadCredentialsException;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.AuthenticationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AuthenticationServiceImplTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationService authenticationService;

    @BeforeEach
    void clearDatabase() {
        userRepository.deleteAll();
    }

    @Test
    void login_happyPath() {
        //given
        UserEntity savedUser = userRepository.save(generateUser(STUDENT));
        UserLoginDto loginDto = createUserLoginDto(savedUser);

        //when
        AuthenticationResponseDto responseDto = authenticationService.login(loginDto.getEmail(), loginDto.getPassword());

        //then
        assertNotNull(responseDto.getToken());
        assertEquals(responseDto.getId(), savedUser.getId());
        assertEquals(responseDto.getUserRole(), savedUser.getUserRole());
        assertEquals(responseDto.getEmail(), savedUser.getEmail());
    }

    @Test
    void login_whenBadCredentials() {
        //given
        userRepository.save(generateUser(STUDENT));
        UserLoginDto loginDto = createUserLoginDtoWithBadCredentials();

        //when-then
        assertThrows(BadCredentialsException.class,
            () -> authenticationService.login(loginDto.getEmail(), loginDto.getPassword()));
    }
}
