package by.itstep.stepaccount.authentication;

import static by.itstep.stepaccount.entity.enums.UserRole.STUDENT;
import static by.itstep.stepaccount.util.DtoGenerationUtils.createUserLoginDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.createUserLoginDtoWithBadCredentials;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import by.itstep.stepaccount.dto.auth.AuthenticationResponseDto;
import by.itstep.stepaccount.dto.auth.UserLoginDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.util.EntityGenerationUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
class AuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void clearDatabase() {
        userRepository.deleteAll();
    }

    @Test
    void login_happyPath() throws Exception {
        //given
        UserEntity savedUser = userRepository.save(EntityGenerationUtils.generateUser(STUDENT));
        UserLoginDto loginDto = createUserLoginDto(savedUser);

        //when
        MvcResult result = mockMvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(loginDto)))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        AuthenticationResponseDto responseDto = objectMapper.readValue(bytes, AuthenticationResponseDto.class);

        //then
        Assertions.assertNotNull(responseDto.getToken());
        Assertions.assertEquals(responseDto.getId(), savedUser.getId());
        Assertions.assertEquals(responseDto.getEmail(), savedUser.getEmail());
        Assertions.assertEquals(responseDto.getUserRole(), savedUser.getUserRole());
    }

    @Test
    void login_whenBadCredentials() throws Exception {
        //given
        userRepository.save(EntityGenerationUtils.generateUser(STUDENT));
        UserLoginDto loginDto = createUserLoginDtoWithBadCredentials();

        //when
        mockMvc.perform(post("/login")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(loginDto)))
            .andExpect(status().isUnauthorized())
            .andReturn();
    }
}
