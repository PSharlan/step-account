package by.itstep.stepaccount.user;

import static by.itstep.stepaccount.util.EntityGenerationUtils.generateStudents;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import by.itstep.stepaccount.entity.RegistrationRequestEntity;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus;
import by.itstep.stepaccount.repository.RegistrationRequestRepository;
import by.itstep.stepaccount.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
class UserRepositoryTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RegistrationRequestRepository registrationRequestRepository;

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        UserEntity toSave = generateUser();
        UserEntity saved = userRepository.save(toSave);

        //when
        Optional<UserEntity> found = userRepository.findById(saved.getId());

        //then
        assertTrue(found.isPresent());
        assertEquals(saved.getId(), found.get().getId());
    }

    @Test
    void findAll_happy_Path() {
        //given
        List<UserEntity> toSave = generateStudents();

        for (UserEntity user : toSave) {
            userRepository.save(user);
        }

        //when
        List<UserEntity> found = userRepository.findAll();

        //then
        assertEquals(toSave.size(), found.size());
    }

    @Test
    void create_happyPath() {
        //given
        UserEntity toSave = generateUser();

        //when
        UserEntity saved = userRepository.save(toSave);

        //then
        assertNotNull(saved);
        assertNotNull(saved.getId());
    }

    @Test
    void update_happyPath() {
        //given
        String newEmail = "updatedEmail";
        UserEntity userToSave = generateUser();

        UserEntity savedUser = userRepository.save(userToSave);
        String email = savedUser.getEmail();
        savedUser.setEmail(newEmail);

        //when
        UserEntity updatedUser = userRepository.save(savedUser);

        //then
        assertNotEquals(email, updatedUser.getEmail());
        assertEquals(newEmail, updatedUser.getEmail());
    }

    @Test
    void deleteById_happy_Path() {
        //given
        UserEntity toSave = generateUser();
        UserEntity saved = userRepository.save(toSave);

        //when
        userRepository.deleteById(saved.getId());

        //then
        assertTrue(userRepository.findById(saved.getId()).isEmpty());
    }

    @Test
    void cascadeTypeDeleteRegistrationRequest_happyPath() {
        //given

        UserEntity userToSave = generateUser();
        UserEntity saved = userRepository.save(userToSave);

        RegistrationRequestEntity registrationRequestEntity = new RegistrationRequestEntity();
        registrationRequestEntity.setStatus(RegistrationRequestEntityStatus.PENDING);
        registrationRequestEntity.setUser(saved);
        RegistrationRequestEntity savedRequest = registrationRequestRepository.save(registrationRequestEntity);

        //when
        userRepository.deleteById(saved.getId());
        Optional<RegistrationRequestEntity> notSupposedToBeFound = registrationRequestRepository.findById(savedRequest.getId());

        //then
        assertTrue(notSupposedToBeFound.isEmpty());
    }
}
