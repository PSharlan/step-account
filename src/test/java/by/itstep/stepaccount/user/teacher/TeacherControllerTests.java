package by.itstep.stepaccount.user.teacher;

import static by.itstep.stepaccount.entity.enums.UserRole.TEACHER;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateTeacherUpdateDto;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateTeachers;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherUpdateDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class TeacherControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void clearDatabase() {
        userRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        UserEntity savedTeacher = userRepository.save(generateUser(TEACHER));

        //when
        MvcResult result = mockMvc.perform(get("/teachers/{id}", savedTeacher.getId()))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TeacherFullDto foundTeacher = objectMapper.readValue(bytes, TeacherFullDto.class);

        //then
        assertNotNull(foundTeacher);
        assertNotNull(foundTeacher.getId());
        assertEquals(savedTeacher.getId(), foundTeacher.getId());
    }

    @Test
    void findById_whenRoleIsNotTeacher() throws Exception {
        //given
        UserEntity userEntityToSave = generateUser();
        userEntityToSave.setUserRole(null);
        UserEntity savedTeacher = userRepository.save(userEntityToSave);

        //when-then
        mockMvc.perform(get("/teachers/{id}", savedTeacher.getId()))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when-then
        mockMvc.perform(get("/teachers/{id}", notExistingId))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<UserEntity> savedTeachers = userRepository.saveAll(generateTeachers());
        Pageable firstPageWithThreeElements = PageRequest.of(0, 3);

        //when
        mockMvc.perform(get("/teachers",firstPageWithThreeElements))
            .andExpect(status().isOk())
            .andReturn();
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        UserEntity teacherToSave = generateUser(TEACHER);

        //when
        MvcResult result = mockMvc.perform(post("/teachers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(teacherToSave)))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TeacherFullDto savedTeacher = objectMapper.readValue(bytes, TeacherFullDto.class);

        //then
        assertNotNull(savedTeacher);
        assertNotNull(savedTeacher.getId());
        assertEquals(teacherToSave.getName(), savedTeacher.getName());
        assertEquals(teacherToSave.getLastName(), savedTeacher.getLastName());
        assertEquals(teacherToSave.getEmail(), savedTeacher.getEmail());
        assertEquals(teacherToSave.getPassword(), savedTeacher.getPassword());
    }

    @Test
    void create_whenEmailIsTaken() throws Exception {
        //given
        UserEntity teacherToSave = generateUser(TEACHER);
        userRepository.save(teacherToSave);
        UserEntity teacherToSave2 = teacherToSave;

        //when-then
        mockMvc.perform(post("/teachers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(teacherToSave2)))
            .andExpect(status().isBadRequest())
            .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        UserEntity savedTeacher = userRepository.save(generateUser(TEACHER));

        TeacherUpdateDto teacherToUpdate = generateTeacherUpdateDto(savedTeacher.getId());

        //when
        MvcResult result = mockMvc.perform(put("/teachers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(teacherToUpdate)))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        TeacherUpdateDto updatedTeacher = objectMapper.readValue(bytes, TeacherUpdateDto.class);

        //then
        assertNotNull(updatedTeacher);
        assertNotNull(updatedTeacher.getId());
        assertEquals(savedTeacher.getId(), updatedTeacher.getId());
        assertNotEquals(savedTeacher.getName(), updatedTeacher.getName());
        assertNotEquals(savedTeacher.getLastName(), updatedTeacher.getLastName());
        assertNotEquals(savedTeacher.getEmail(), updatedTeacher.getEmail());
        assertNotEquals(savedTeacher.getPassword(), updatedTeacher.getPassword());
    }

    @Test
    void update_whenRoleIsNotTeacher() throws Exception {
        //given
        UserEntity savedTeacher = userRepository.save(generateUser());
        savedTeacher.setUserRole(null);

        TeacherUpdateDto teacherToUpdate = generateTeacherUpdateDto(savedTeacher.getId());

        //when-then
        mockMvc.perform(put("/teachers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(teacherToUpdate)))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void update_whenNotFound() throws Exception {
        //given
        UserEntity savedTeacher = userRepository.save(generateUser(TEACHER));

        TeacherUpdateDto teacherToUpdate = generateTeacherUpdateDto(savedTeacher.getId());
        Integer notExistingId = 1000;
        teacherToUpdate.setId(notExistingId);

        //when-then
        mockMvc.perform(put("/teachers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(teacherToUpdate)))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        UserEntity savedTeacher = userRepository.save(generateUser(TEACHER));

        //when
        mockMvc.perform(delete("/teachers/{id}", savedTeacher.getId()))
            .andExpect(status().isOk());

        //then
        mockMvc.perform(delete("/teachers/{id}", savedTeacher.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when-then
        mockMvc.perform(delete("/teachers/{id}", notExistingId))
            .andExpect(status().isNotFound());
    }
}
