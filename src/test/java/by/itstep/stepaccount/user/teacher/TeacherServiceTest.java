package by.itstep.stepaccount.user.teacher;

import static by.itstep.stepaccount.entity.enums.UserRole.TEACHER;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateTeacherCreateDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateTeacherUpdateDto;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateTeachers;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import by.itstep.stepaccount.dto.teacher.TeacherCreateDto;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherPreviewDto;
import by.itstep.stepaccount.dto.teacher.TeacherUpdateDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.exception.EntityIsNotFoundException;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.TeacherService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import java.util.List;

@SpringBootTest
public class TeacherServiceTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeacherService teacherService;

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        UserEntity userEntity = generateUser(TEACHER);

        UserEntity saved = userRepository.save(userEntity);

        //when
        TeacherFullDto foundTeacher = teacherService.findById(saved.getId());

        //then
        assertNotNull(foundTeacher);
        assertEquals(saved.getId(), foundTeacher.getId());
    }

    @Test
    void findById_whenNotFound() {
        //given
        Integer notExistingId = 1000;

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
            () -> teacherService.findById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void findAll_happyPath() {
        //given
        List<UserEntity> teachersSaved = userRepository.saveAll(generateTeachers());

        //when
        List<TeacherPreviewDto> teachersFound = teacherService.findAll(Pageable.unpaged()).getContent();

        //then
        assertEquals(teachersSaved.size(), teachersFound.size());
        for (int i = 0; i < teachersFound.size(); i++) {
            assertEquals(teachersFound.get(i).getId(), teachersSaved.get(i).getId());
            assertEquals(teachersFound.get(i).getName(), teachersSaved.get(i).getName());
            assertEquals(teachersFound.get(i).getLastName(), teachersSaved.get(i).getLastName());
        }
    }

    @Test
    void create_happyPath() {
        //given
        TeacherCreateDto teacherToSave = generateTeacherCreateDto();

        //when
        TeacherFullDto savedTeacher = teacherService.create(teacherToSave);

        //then
        assertNotNull(savedTeacher.getId());
        assertEquals(savedTeacher.getName(), teacherToSave.getName());
        assertEquals(savedTeacher.getLastName(), teacherToSave.getLastName());
        assertEquals(savedTeacher.getEmail(), teacherToSave.getEmail());
        assertEquals(savedTeacher.getPassword(), teacherToSave.getPassword());
    }

    @Test
    void update_happyPath() {
        //given
        TeacherCreateDto teacherToCreate = generateTeacherCreateDto();
        TeacherFullDto savedTeacher = teacherService.create(teacherToCreate);

        TeacherUpdateDto teacherToUpdate = generateTeacherUpdateDto(savedTeacher.getId());

        //when
        TeacherFullDto updatedTeacher = teacherService.update(teacherToUpdate);

        //then
        assertNotNull(updatedTeacher);
        assertEquals(savedTeacher.getId(), updatedTeacher.getId());
        assertNotEquals(savedTeacher.getName(), updatedTeacher.getName());
        assertNotEquals(savedTeacher.getLastName(), updatedTeacher.getLastName());
        assertNotEquals(savedTeacher.getEmail(), updatedTeacher.getEmail());
        assertNotEquals(savedTeacher.getPassword(), updatedTeacher.getPassword());
    }

    @Test
    void update_whenNotFound() {
        //given
        TeacherCreateDto teacherToCreate = generateTeacherCreateDto();
        TeacherFullDto savedTeacher = teacherService.create(teacherToCreate);

        TeacherUpdateDto teacherToUpdate = generateTeacherUpdateDto(savedTeacher.getId());

        Integer notExistingId = 1000;
        teacherToUpdate.setId(notExistingId);

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
            () -> teacherService.update(teacherToUpdate));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void delete_happyPath() {
        //given
        TeacherFullDto savedTeacher = teacherService.create(generateTeacherCreateDto());

        //when
        teacherService.deleteById(savedTeacher.getId());

        //then
        assertThrows(RuntimeException.class,
            () -> teacherService.findById(savedTeacher.getId()));
    }

    @Test
    void deleteById_whenNotFound() {
        //given
        Integer notExisting = 1000;

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
            () -> teacherService.deleteById(notExisting));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
    }
}
