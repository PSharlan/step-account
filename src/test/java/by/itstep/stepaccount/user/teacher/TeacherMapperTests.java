package by.itstep.stepaccount.user.teacher;

import static by.itstep.stepaccount.entity.enums.UserRole.TEACHER;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateTeacherCreateDto;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateTeachers;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import by.itstep.stepaccount.dto.teacher.TeacherCreateDto;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherPreviewDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.mapper.TeacherMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TeacherMapperTests {

    @Autowired
    private TeacherMapper mapper;

    @Test
    public void givenEntityToFullDto_whenMaps_thenCorrect() {
        //given
        UserEntity teacherEntity = generateUser();
        teacherEntity.setUserRole(TEACHER);

        //when
        TeacherFullDto teacherFullDto = mapper.mapToFullDto(teacherEntity);

        //then
        assertEquals(teacherEntity.getName(), teacherFullDto.getName());
        assertEquals(teacherEntity.getLastName(), teacherFullDto.getLastName());
        assertEquals(teacherEntity.getEmail(), teacherFullDto.getEmail());
        assertEquals(teacherEntity.getLastLoginDate(), teacherFullDto.getLastLoginDate());
        assertEquals(teacherEntity.getPassword(), teacherFullDto.getPassword());
        assertEquals(teacherEntity.getRegistrationDate(), teacherFullDto.getRegistrationDate());
    }

    @Test
    public void givenCreateDtoToEntity_whenMaps_thenCorrect() {
        //given
        TeacherCreateDto teacherCreateDto = generateTeacherCreateDto();

        //when
        UserEntity teacherEntity = mapper.mapToEntity(teacherCreateDto);

        //then
        assertEquals(teacherCreateDto.getName(), teacherEntity.getName());
        assertEquals(teacherCreateDto.getLastName(), teacherEntity.getLastName());
        assertEquals(teacherCreateDto.getPassword(), teacherEntity.getPassword());
        assertEquals(teacherCreateDto.getEmail(), teacherEntity.getEmail());
    }

    @Test
    public void givenListEntityToListPreviewDto_whenMaps_thenCorrect() {
        //given
        List<UserEntity> teacherEntities = generateTeachers();

        //when
        List<TeacherPreviewDto> teacherPreviewDtos = mapper.mapToPreviewDtoList(teacherEntities);

        //then
        for (int i = 0; i < teacherEntities.size(); i++) {
            assertEquals(teacherEntities.get(i).getName(), teacherPreviewDtos.get(i).getName());
            assertEquals(teacherEntities.get(i).getId(), teacherPreviewDtos.get(i).getId());
            assertEquals(teacherEntities.get(i).getLastName(), teacherPreviewDtos.get(i).getLastName());
            assertEquals(teacherEntities.get(i).getEmail(), teacherPreviewDtos.get(i).getEmail());
        }
    }
}
