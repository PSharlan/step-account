package by.itstep.stepaccount.user.student;

import static by.itstep.stepaccount.entity.enums.UserRole.STUDENT;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateStudentCreateDto;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateStudents;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.dto.student.StudentPreviewDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.mapper.StudentMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class StudentMapperTests {

    @Autowired
    private StudentMapper mapper;

    @Test
    void givenEntityToFullDto_whenMaps_thenCorrect() {
        //given
        UserEntity studentEntity = generateUser();
        studentEntity.setUserRole(STUDENT);

        //when
        StudentFullDto studentFullDto = mapper.mapToFullDto(studentEntity);

        //then
        assertEquals(studentEntity.getName(), studentFullDto.getName());
        assertEquals(studentEntity.getEmail(), studentFullDto.getEmail());
        assertEquals(studentEntity.getLastName(), studentFullDto.getLastName());
        assertEquals(studentEntity.getRegistrationDate(), studentFullDto.getRegistrationDate());
        assertEquals(studentEntity.getUserRole(), studentFullDto.getUserRole());
    }

    @Test
    void givenCreateDtoToEntity_whenMaps_thenCorrect() {
        //given
        StudentCreateDto studentCreateDto = generateStudentCreateDto();

        //when
        UserEntity studentEntity = mapper.mapToEntity(studentCreateDto);

        //then
        assertEquals(studentCreateDto.getName(), studentEntity.getName());
        assertEquals(studentCreateDto.getLastName(), studentEntity.getLastName());
        assertEquals(studentCreateDto.getEmail(), studentEntity.getEmail());
        assertEquals(studentCreateDto.getPassword(), studentEntity.getPassword());
    }

    @Test
    void givenListEntityToListPreviewDto_whenMaps_thenCorrect() {
        //given
        List<UserEntity> students = generateStudents();

        //when
        List<StudentPreviewDto> groupPreviewDtos = mapper.mapToPreviewDtoList(students);

        //then
        for (int i = 0; i < students.size(); i++) {
            assertEquals(students.get(i).getName(), groupPreviewDtos.get(i).getName());
            assertEquals(students.get(i).getLastName(), groupPreviewDtos.get(i).getLastName());
            assertEquals(students.get(i).getEmail(), groupPreviewDtos.get(i).getEmail());
        }
    }
}
