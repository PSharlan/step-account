package by.itstep.stepaccount.user.student;

import static by.itstep.stepaccount.entity.enums.UserRole.STUDENT;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateGroupCreateDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateStudentCreateDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateStudentUpdateDto;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateStudents;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import by.itstep.stepaccount.dto.group.GroupFullDto;
import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.dto.student.StudentPreviewDto;
import by.itstep.stepaccount.dto.student.StudentUpdateDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.exception.EntityIsNotFoundException;
import by.itstep.stepaccount.repository.GroupRepository;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.GroupService;
import by.itstep.stepaccount.service.StudentService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import java.util.List;

@SpringBootTest
class StudentServiceTests {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private StudentService studentService;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private GroupService groupService;

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
        groupRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        UserEntity userEntity = generateUser(STUDENT);

        UserEntity saved = userRepository.save(userEntity);

        //when
        StudentFullDto foundDto = studentService.findById(saved.getId());

        //then
        assertNotNull(foundDto);
        assertEquals(saved.getId(), foundDto.getId());
    }

    @Test
    void findById_whenNotFound() {
        //given
        Integer notExistingId = 1000;

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
            () -> studentService.findById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void findAll_happyPath() {
        //given
        List<UserEntity> saved = userRepository.saveAll(generateStudents());

        //when
        List<StudentPreviewDto> foundStudents = studentService.findAll(Pageable.unpaged()).getContent();

        //then
        assertEquals(foundStudents.size(), saved.size());
        for (int i = 0; i < foundStudents.size(); i++) {
            assertEquals(foundStudents.get(i).getId(), saved.get(i).getId());
            assertEquals(foundStudents.get(i).getName(), saved.get(i).getName());
            assertEquals(foundStudents.get(i).getLastName(), saved.get(i).getLastName());
        }
    }

    @Test
    void create_happyPath() {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());
        StudentCreateDto studentToSave = generateStudentCreateDto(savedGroup.getId());

        //when
        StudentFullDto saved = studentService.create(studentToSave);

        //then
        assertNotNull(saved.getId());
        assertEquals(saved.getName(), studentToSave.getName());
        assertEquals(saved.getLastName(), studentToSave.getLastName());
        assertEquals(saved.getEmail(), studentToSave.getEmail());
        assertEquals(saved.getUserRole(), STUDENT);
        assertNotNull(saved.getRegistrationDate());
        assertEquals(studentToSave.getGroupId(), saved.getGroup().getId());
    }

    @Test
    void create_whenGroupNotFound() {
        //given
        Integer notExistingGroupId = 1000;
        StudentCreateDto savedStudent = generateStudentCreateDto(notExistingGroupId);

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
            () -> studentService.create(savedStudent));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingGroupId)));
    }

    @Test
    void update_happyPath() {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());
        StudentCreateDto studentCreateDto = generateStudentCreateDto(savedGroup.getId());

        StudentFullDto createdStudent = studentService.create(studentCreateDto);

        StudentUpdateDto updateDto = generateStudentUpdateDto(createdStudent.getId(), savedGroup.getId());

        //when
        StudentFullDto updatedStudent = studentService.update(updateDto);

        //then
        assertNotNull(updatedStudent);
        assertEquals(createdStudent.getId(), updatedStudent.getId());
        assertEquals(createdStudent.getGroup(), updatedStudent.getGroup());
        assertNotEquals(createdStudent.getName(), updatedStudent.getName());
    }

    @Test
    void update_whenNotFound() {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());
        StudentCreateDto studentCreateDto = generateStudentCreateDto(savedGroup.getId());

        StudentFullDto createdStudent = studentService.create(studentCreateDto);

        StudentUpdateDto updateDto = generateStudentUpdateDto(createdStudent.getId(), savedGroup.getId());

        Integer notExistingId = 1000;
        updateDto.setId(notExistingId);

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
            () -> studentService.update(updateDto));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenGroupNotFound() {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());
        StudentCreateDto studentCreateDto = generateStudentCreateDto(savedGroup.getId());

        StudentFullDto createdStudent = studentService.create(studentCreateDto);

        Integer notExistingGroupId = 1000;
        StudentUpdateDto updateDto = generateStudentUpdateDto(createdStudent.getId(), notExistingGroupId);

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
            () -> studentService.update(updateDto));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingGroupId)));
    }

    @Test
    void deleteById_happyPath() {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());
        StudentCreateDto studentToSave = generateStudentCreateDto(savedGroup.getId());

        StudentFullDto savedStudent = studentService.create(studentToSave);

        //when
        studentService.deleteById(savedStudent.getId());

        //then
        assertThrows(EntityIsNotFoundException.class,
            () -> studentService.findById(savedStudent.getId()));
    }

    @Test
    void deleteById_whenNotFound() {
        //given
        Integer notExisting = 1000;

        //when
        Exception exception = assertThrows(RuntimeException.class,
            () -> studentService.deleteById(notExisting));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
    }
}
