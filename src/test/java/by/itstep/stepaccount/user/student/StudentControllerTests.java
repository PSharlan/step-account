package by.itstep.stepaccount.user.student;

import static by.itstep.stepaccount.entity.enums.UserRole.STUDENT;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateGroupCreateDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateStudentCreateDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateStudentUpdateDto;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateStudents;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import by.itstep.stepaccount.dto.group.GroupFullDto;
import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.dto.student.StudentUpdateDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.repository.GroupRepository;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.GroupService;
import by.itstep.stepaccount.service.StudentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupService groupService;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private StudentService studentService;

    @BeforeEach
    void clearDatabase() {
        userRepository.deleteAll();
        groupRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        UserEntity savedStudent = userRepository.save(generateUser(STUDENT));

        //when
        MvcResult result = mockMvc.perform(get("/students/{id}", savedStudent.getId()))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        StudentFullDto foundStudent = objectMapper.readValue(bytes, StudentFullDto.class);

        //then
        assertNotNull(foundStudent);
        assertNotNull(foundStudent.getId());
        assertEquals(savedStudent.getId(), foundStudent.getId());
    }

    @Test
    void findById_whenRoleIsNotStudent() throws Exception {
        //given
        UserEntity userEntityToSave = generateUser();
        userEntityToSave.setUserRole(null);
        UserEntity savedStudent = userRepository.save(userEntityToSave);

        //when-then
        mockMvc.perform(get("/students/{id}", savedStudent.getId()))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when-then
        mockMvc.perform(get("/students/{id}", notExistingId))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<UserEntity> savedStudents = userRepository.saveAll(generateStudents());
        Pageable firstPageWithThreeElements = PageRequest.of(0, 3);

        //when
        mockMvc.perform(get("/students",firstPageWithThreeElements))
            .andExpect(status().isOk())
            .andReturn();
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());
        StudentCreateDto studentToSave = generateStudentCreateDto(savedGroup.getId());

        //when
        MvcResult result = mockMvc.perform(post("/students")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(studentToSave)))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        StudentFullDto savedStudent = objectMapper.readValue(bytes, StudentFullDto.class);

        //then
        assertNotNull(savedStudent);
        assertNotNull(savedStudent.getId());
        assertEquals(studentToSave.getName(), savedStudent.getName());
        assertEquals(studentToSave.getLastName(), savedStudent.getLastName());
        assertEquals(studentToSave.getEmail(), savedStudent.getEmail());
        assertEquals(studentToSave.getGroupId(), savedStudent.getGroup().getId());
    }

    @Test
    void create_whenEmailIsTaken() throws Exception {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());
        StudentCreateDto studentToSave = generateStudentCreateDto(savedGroup.getId());
        studentService.create(studentToSave);

        StudentCreateDto studentToSave2 = generateStudentCreateDto(savedGroup.getId());

        //when-then
        mockMvc.perform(post("/students")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(studentToSave2)))
            .andExpect(status().isBadRequest())
            .andReturn();
    }

    @Test
    void create_whenGroupNotFound() throws Exception {
        //given
        StudentCreateDto studentToSave = generateStudentCreateDto();

        //when-then
        mockMvc.perform(post("/students")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(studentToSave)))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());

        StudentCreateDto studentToSave = generateStudentCreateDto(savedGroup.getId());

        StudentFullDto savedStudent = studentService.create(studentToSave);

        StudentUpdateDto studentToUpdate = generateStudentUpdateDto(savedStudent.getId(),
            savedStudent.getGroup().getId());

        //when
        MvcResult result = mockMvc.perform(put("/students")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(studentToUpdate)))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        StudentUpdateDto updatedStudent = objectMapper.readValue(bytes, StudentUpdateDto.class);

        //then
        assertNotNull(updatedStudent);
        assertNotNull(updatedStudent.getId());
        assertEquals(savedStudent.getId(), updatedStudent.getId());
        assertNotEquals(savedStudent.getName(), updatedStudent.getName());
        assertNotEquals(savedStudent.getLastName(), updatedStudent.getLastName());
    }

    @Test
    void update_whenRoleIsNotStudent() throws Exception {
        //given
        UserEntity userEntityToSave = generateUser();
        userEntityToSave.setUserRole(null);
        UserEntity userEntityToUpdate = userRepository.save(userEntityToSave);

        //when-then
        mockMvc.perform(put("/students")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(userEntityToUpdate)))
            .andExpect(status().isBadRequest())
            .andReturn();
    }

    @Test
    void update_whenNotFound() throws Exception {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());

        StudentCreateDto studentToSave = generateStudentCreateDto(savedGroup.getId());

        StudentFullDto savedStudent = studentService.create(studentToSave);

        StudentUpdateDto studentToUpdate = generateStudentUpdateDto(savedStudent.getId(),
            savedStudent.getGroup().getId());
        Integer idNotExisting = 1000;
        studentToUpdate.setId(idNotExisting);

        //when-then
        mockMvc.perform(put("/students")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(studentToUpdate)))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        UserEntity savedStudent = userRepository.save(generateUser(STUDENT));

        //when
        mockMvc.perform(delete("/students/{id}", savedStudent.getId()))
            .andExpect(status().isOk());

        //then
        mockMvc.perform(delete("/students/{id}", savedStudent.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when-then
        mockMvc.perform(delete("/students/{id}", notExistingId))
            .andExpect(status().isNotFound());
    }
}
