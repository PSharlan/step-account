package by.itstep.stepaccount.user.student;

import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.impl.StudentServiceImpl;
import by.itstep.stepaccount.util.EntityGenerationUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class StudentServiceTest {

    @Autowired
    private StudentServiceImpl studentService;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    private void setUp(){
        userRepository.deleteAll();
    }

    @Test
    void findById_happyPath(){
        //given
        UserEntity entitySaved = userRepository.save(EntityGenerationUtils.generateUser());

        //when
        StudentFullDto foundById = studentService.findById(entitySaved.getId());

        //then
        Assertions.assertEquals(entitySaved.getId(),foundById.getId());
    }

    @Test
    void findById_whenNotFound(){
        //given
        Integer notExistingId = 13;

        //when-then
        Assertions.assertThrows(RuntimeException.class,
            () -> studentService.findById(notExistingId));
    }
}
