package by.itstep.stepaccount.group;

import static by.itstep.stepaccount.entity.enums.Subject.GEOGRAPHY;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateGroup;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateListGroup;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import by.itstep.stepaccount.entity.GroupEntity;
import by.itstep.stepaccount.entity.enums.Subject;
import by.itstep.stepaccount.repository.GroupRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
class GroupRepositoryTests {

    @Autowired
    private GroupRepository groupRepository;

    @BeforeEach
    void setUp() {
        groupRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        GroupEntity toSave = generateGroup();
        GroupEntity saved = groupRepository.save(toSave);

        //when
        Optional<GroupEntity> found = groupRepository.findById(saved.getId());

        //then
        assertTrue(found.isPresent());
        assertEquals(saved.getId(), found.get().getId());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<GroupEntity> toSave = generateListGroup();

        for (GroupEntity group : toSave) {
            groupRepository.save(group);
        }

        //when
        List<GroupEntity> found = groupRepository.findAll();

        //then
        assertEquals(toSave.size(), found.size());
    }

    @Test
    void create_happyPath() {
        //given
        GroupEntity toSave = generateGroup();

        //when
        GroupEntity saved = groupRepository.save(toSave);

        //then
        assertNotNull(saved);
        assertNotNull(saved.getId());
    }

    @Test
    void testUpdate_happyPath() {
        //given
        Subject newSubject = GEOGRAPHY;
        GroupEntity groupToSave = generateGroup();

        GroupEntity savedGroup = groupRepository.save(groupToSave);
        Subject subject = savedGroup.getSubjectOfStudy();
        savedGroup.setSubjectOfStudy(newSubject);

        //when
        GroupEntity updatedGroup = groupRepository.save(savedGroup);

        //then
        assertNotEquals(subject, updatedGroup.getSubjectOfStudy());
        assertEquals(newSubject, updatedGroup.getSubjectOfStudy());
    }

    @Test
    void deleteById_happyPath() {
        //given
        GroupEntity toSave = generateGroup();
        GroupEntity saved = groupRepository.save(toSave);
        Integer savedGroupId = saved.getId();

        //when
        groupRepository.deleteById(savedGroupId);

        //then
        assertTrue(groupRepository.findById(savedGroupId).isEmpty());
    }
}
