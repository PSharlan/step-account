package by.itstep.stepaccount.group;

import static by.itstep.stepaccount.util.DtoGenerationUtils.generateGroupCreateDto;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateGroup;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateListGroup;
import static org.junit.jupiter.api.Assertions.assertEquals;
import by.itstep.stepaccount.dto.group.GroupCreateDto;
import by.itstep.stepaccount.dto.group.GroupFullDto;
import by.itstep.stepaccount.dto.group.GroupPreviewDto;
import by.itstep.stepaccount.entity.GroupEntity;
import by.itstep.stepaccount.mapper.GroupMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class GroupMapperTests {

    @Autowired
    private GroupMapper groupMapper;

    @Test
    void givenEntityToFullDto_whenMaps_thenCorrect() {
        //given
        GroupEntity groupEntity = generateGroup();

        //when
        GroupFullDto groupFullDto = groupMapper.mapToFullDto(groupEntity);

        //then
        assertEquals(groupEntity.getName(), groupFullDto.getName());
        assertEquals(groupEntity.getStartDate(), groupFullDto.getStartDate());
        assertEquals(groupEntity.getSubjectOfStudy(), groupFullDto.getSubjectOfStudy());
        assertEquals(groupEntity.getTrainingPeriod(), groupFullDto.getTrainingPeriod());
    }

    @Test
    void givenCreateDtoToEntity_whenMaps_thenCorrect() {
        //given
        GroupCreateDto createDto = generateGroupCreateDto();

        //when
        GroupEntity groupEntity = groupMapper.mapToEntity(createDto);

        //then
        assertEquals(createDto.getStartDate(), groupEntity.getStartDate());
        assertEquals(createDto.getTrainingPeriod(), groupEntity.getTrainingPeriod());
        assertEquals(createDto.getSubjectOfStudy(), groupEntity.getSubjectOfStudy());
    }

    @Test
    void givenListEntityToListPreviewDto_whenMaps_thenCorrect() {
        //given
        List<GroupEntity> groupEntities = generateListGroup();

        //when
        List<GroupPreviewDto> groupPreviewDtos = groupMapper.mapToPreviewDtoList(groupEntities);

        //then
        for (int i = 0; i < groupEntities.size(); i++) {
            assertEquals(groupEntities.get(i).getName(), groupPreviewDtos.get(i).getName());
            assertEquals(groupEntities.get(i).getStartDate(), groupPreviewDtos.get(i).getStartDate());
            assertEquals(groupEntities.get(i).getSubjectOfStudy(), groupPreviewDtos.get(i).getSubjectOfStudy());
        }
    }
}
