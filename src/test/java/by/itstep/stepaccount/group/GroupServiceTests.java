package by.itstep.stepaccount.group;

import static by.itstep.stepaccount.util.DtoGenerationUtils.*;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateGroup;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateListGroup;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import by.itstep.stepaccount.dto.group.GroupCreateDto;
import by.itstep.stepaccount.dto.group.GroupFullDto;
import by.itstep.stepaccount.dto.group.GroupPreviewDto;
import by.itstep.stepaccount.dto.group.GroupUpdateDto;
import by.itstep.stepaccount.dto.registrationRequest.RegistrationRequestFullDto;
import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherCreateDto;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.entity.GroupEntity;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.entity.enums.UserRole;
import by.itstep.stepaccount.exception.EntityIsNotFoundException;
import by.itstep.stepaccount.repository.GroupRepository;
import by.itstep.stepaccount.repository.RegistrationRequestRepository;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.GroupService;
import by.itstep.stepaccount.service.RegistrationRequestService;
import by.itstep.stepaccount.service.StudentService;
import by.itstep.stepaccount.service.TeacherService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import java.util.List;

@SpringBootTest
class GroupServiceTests {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private GroupService groupService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private RegistrationRequestService requestService;

    @Autowired
    private RegistrationRequestRepository requestRepository;

    @BeforeEach
    void setUp() {
        groupRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        GroupEntity groupSaved = groupRepository.save(generateGroup());

        //when
        GroupFullDto groupFound = groupService.findById(groupSaved.getId());

        //then
        assertNotNull(groupFound);
        assertEquals(groupSaved.getId(), groupFound.getId());
    }

    @Test
    void findById_whereAllStudentsIsNotConfirmed_happyPath() {
        //given
        GroupEntity groupSaved = groupRepository.save(generateGroup());
        setThreeStudentWithRequestStatusIsPending(groupSaved);

        //when
        GroupFullDto groupFound = groupService.findById(groupSaved.getId());
        System.out.println(groupFound.getStudents());

        //then
        assertEquals(0, groupFound.getStudents().size());
        assertNotNull(groupFound);
        assertEquals(groupSaved.getId(), groupFound.getId());
        clearDataBaseAuxiliary();
    }

    @Test
    void findById_whereHalfStudentsIsNotConfirmed_happyPath() {
        //given
        GroupEntity groupSaved = groupRepository.save(generateGroup());
        setThreeStudentWithRequestStatusIsPending(groupSaved);
        setThreeStudentWithRequestStatusIsAccepted(groupSaved);

        //when
        GroupFullDto groupFound = groupService.findById(groupSaved.getId());
        System.out.println(groupFound.getStudents());

        //then
        assertEquals(3, groupFound.getStudents().size());
        assertNotNull(groupFound);
        assertEquals(groupSaved.getId(), groupFound.getId());
        clearDataBaseAuxiliary();
    }

    @Test
    void findById_whenNotFound() {
        //given
        Integer notExistingId = 1000;

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
                () -> groupService.findById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void findByName_happyPath() {
        //given
        GroupEntity groupSaved = groupRepository.save(generateGroup());

        //when
        GroupFullDto groupFound = groupService.findByName(groupSaved.getName());

        //then
        assertNotNull(groupFound);
        assertEquals(groupSaved.getId(), groupFound.getId());
    }

    @Test
    void findByName_whereAllStudentsIsNotConfirmed_happyPath() {
        //given
        GroupEntity groupSaved = groupRepository.save(generateGroup());
        setThreeStudentWithRequestStatusIsPending(groupSaved);

        //when
        GroupFullDto groupFound = groupService.findByName(groupSaved.getName());
        System.out.println(groupFound.getStudents());

        //then
        assertEquals(0, groupFound.getStudents().size());
        assertNotNull(groupFound);
        assertEquals(groupSaved.getId(), groupFound.getId());
        clearDataBaseAuxiliary();
    }

    @Test
    void findByName_whereHalfStudentsIsNotConfirmed_happyPath() {
        //given
        GroupEntity groupSaved = groupRepository.save(generateGroup());
        setThreeStudentWithRequestStatusIsPending(groupSaved);
        setThreeStudentWithRequestStatusIsAccepted(groupSaved);

        //when
        GroupFullDto groupFound = groupService.findByName(groupSaved.getName());
        System.out.println(groupFound.getStudents());

        //then
        assertEquals(3, groupFound.getStudents().size());
        assertNotNull(groupFound);
        assertEquals(groupSaved.getId(), groupFound.getId());
        clearDataBaseAuxiliary();
    }

    @Test
    void findAll_happyPath() {
        //given
        List<GroupEntity> groupsSaved = groupRepository.saveAll(generateListGroup());

        //when
        List<GroupPreviewDto> groupsFound = groupService.findAll(Pageable.unpaged()).getContent();

        //then
        assertEquals(groupsSaved.size(), groupsFound.size());
    }

    @Test
    void findAllByUserId_forTeacher_happyPath() {
        //given
        UserEntity teacher = userRepository.save(generateUser(UserRole.TEACHER));
        RegistrationRequestFullDto request = requestService.createRequest(teacher);
        requestService.confirmRequest(request.getId());


        List<GroupEntity> groupEntities = generateListGroup();
        for (GroupEntity group : groupEntities) {
            group.setTeacher(teacher);
        }

        List<GroupEntity> savedGroups = groupRepository.saveAll(groupEntities);

        //when
        List<GroupPreviewDto> groupsFound = groupService.findAllByUserId(teacher.getId());

        //then
        assertEquals(savedGroups.size(), groupsFound.size());
    }

    @Test
    void create_happyPath() {
        //given
        GroupCreateDto groupToSave = generateGroupCreateDto();

        //when
        GroupFullDto savedGroup = groupService.create(groupToSave);

        //then
        assertNotNull(savedGroup.getId());
        assertEquals(groupToSave.getName(), savedGroup.getName());
        assertEquals(groupToSave.getSubjectOfStudy(), savedGroup.getSubjectOfStudy());
        assertEquals(groupToSave.getStartDate(), savedGroup.getStartDate());
        assertEquals(groupToSave.getTrainingPeriod(), savedGroup.getTrainingPeriod());
    }

    @Test
    void update_happyPath() {
        //given
        GroupCreateDto groupCreateDto = generateGroupCreateDto();

        GroupFullDto groupCreated = groupService.create(groupCreateDto);

        GroupUpdateDto groupUpdateDto = generateGroupUpdateDto(groupCreated.getId(),
                groupCreated.getStartDate());

        TeacherCreateDto teacherCreateDto = generateTeacherCreateDto();
        TeacherFullDto savedTeacher = teacherService.create(teacherCreateDto);
        groupUpdateDto.setTeacherId(savedTeacher.getId());

        //when
        GroupFullDto groupUpdated = groupService.update(groupUpdateDto);

        //then
        assertNotNull(groupUpdated);
        assertEquals(groupCreated.getId(), groupUpdated.getId());
        assertNotEquals(groupCreated.getName(), groupUpdated.getName());
        assertNotEquals(groupCreated.getSubjectOfStudy(), groupUpdated.getSubjectOfStudy());
        assertEquals(groupCreated.getStartDate(), groupUpdated.getStartDate());
        assertNotEquals(groupCreated.getTrainingPeriod(), groupUpdated.getTrainingPeriod());
    }

    @Test
    void update_whenNotFound() {
        //given
        GroupCreateDto groupCreateDto = generateGroupCreateDto();

        GroupFullDto groupCreated = groupService.create(groupCreateDto);

        GroupUpdateDto groupUpdateDto = generateGroupUpdateDto(groupCreated.getId(),
                groupCreated.getStartDate());

        Integer notExistingId = 1000;
        groupUpdateDto.setId(notExistingId);

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
                () -> groupService.update(groupUpdateDto));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void update_whenTeacherNotFound() {
        //given
        GroupCreateDto groupCreateDto = generateGroupCreateDto();

        GroupFullDto groupCreated = groupService.create(groupCreateDto);

        GroupUpdateDto groupUpdateDto = generateGroupUpdateDto(groupCreated.getId(),
                groupCreated.getStartDate());

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
                () -> groupService.update(groupUpdateDto));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(groupUpdateDto.getTeacherId())));
    }

    @Test
    void deleteById_happyPath() {
        //given
        GroupFullDto savedGroup = groupService.create(generateGroupCreateDto());
        Integer savedGroupId = savedGroup.getId();

        //when
        groupService.deleteById(savedGroupId);

        //then
        assertThrows(EntityIsNotFoundException.class,
                () -> groupService.findById(savedGroupId));
    }

    @Test
    void deleteById_whenNotFound() {
        //given
        Integer notExisting = 1000;

        //when
        Exception exception = assertThrows(EntityIsNotFoundException.class,
                () -> groupService.deleteById(notExisting));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
    }

    private void setThreeStudentWithRequestStatusIsPending(GroupEntity groupSaved) {
        for (int i = 0; i < 3; i++) {
            StudentCreateDto studentCreateDto = generateStudentCreateDto();
            studentCreateDto.setGroupId(groupSaved.getId());
            studentService.create(studentCreateDto);
        }
    }

    private void setThreeStudentWithRequestStatusIsAccepted(GroupEntity groupSaved) {
        for (int i = 0; i < 3; i++) {
            StudentCreateDto studentCreateDto = generateStudentCreateDto();
            studentCreateDto.setGroupId(groupSaved.getId());
            StudentFullDto createdStudent = studentService.create(studentCreateDto);
            requestService.confirmRequest(requestRepository.findByUserId(createdStudent.getId()).getId());
        }
    }

    private void clearDataBaseAuxiliary() {
        requestRepository.deleteAll();
        userRepository.deleteAll();
    }
}

