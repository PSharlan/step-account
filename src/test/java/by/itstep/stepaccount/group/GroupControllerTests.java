package by.itstep.stepaccount.group;

import static by.itstep.stepaccount.entity.enums.UserRole.STUDENT;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateGroupCreateDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateGroupUpdateDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateTeacherCreateDto;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateGroup;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateListGroup;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import by.itstep.stepaccount.dto.group.GroupCreateDto;
import by.itstep.stepaccount.dto.group.GroupFullDto;
import by.itstep.stepaccount.dto.group.GroupUpdateDto;
import by.itstep.stepaccount.dto.registrationRequest.RegistrationRequestFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherCreateDto;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.entity.GroupEntity;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.repository.GroupRepository;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.RegistrationRequestService;
import by.itstep.stepaccount.service.TeacherService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class GroupControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RegistrationRequestService requestService;

    @BeforeEach
    void clearDatabase() {
        groupRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        GroupEntity savedGroup = groupRepository.save(generateGroup());

        //when
        MvcResult result = mockMvc.perform(get("/groups/{id}", savedGroup.getId()))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        GroupFullDto foundGroup = objectMapper.readValue(bytes, GroupFullDto.class);

        //then
        assertNotNull(foundGroup);
        assertNotNull(foundGroup.getId());
        assertEquals(savedGroup.getId(), foundGroup.getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        Integer idNotExisting = 10000;

        //when-then
        mockMvc.perform(get("/groups/{id}", idNotExisting))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<GroupEntity> savedGroups = groupRepository.saveAll(generateListGroup());
        Pageable firstPageWithThreeElements = PageRequest.of(0, 3);

        //when
        mockMvc.perform(get("/groups",firstPageWithThreeElements))
            .andExpect(status().isOk())
            .andReturn();
    }

    @Test
    void findAllByUserId_forStudents_happyPath() throws Exception {
        //given
        List<GroupEntity> savedGroups = (List.of(groupRepository.save(generateGroup())));
        UserEntity student = generateUser(STUDENT);
        student.setGroup(savedGroups.get(0));
        UserEntity savedStudent = userRepository.save(student);
        RegistrationRequestFullDto request = requestService.createRequest(savedStudent);
        requestService.confirmRequest(request.getId());

        //when
        MvcResult result = mockMvc.perform(get("/groups/users/{userId}", savedStudent.getId()))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<GroupEntity> foundGroups = objectMapper.readValue(bytes, new TypeReference<>() {});

        //then
        assertEquals(savedGroups.size(), foundGroups.size());
        assertEquals(foundGroups.get(0).getId(), savedStudent.getGroup().getId());
        userRepository.deleteAll(); //crutch for normal passing of tests
    }

    @Test
    void create_happyPath() throws Exception {
        //given
        GroupCreateDto groupToSave = generateGroupCreateDto();

        //when
        MvcResult result = mockMvc.perform(post("/groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(groupToSave)))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        GroupFullDto savedGroup = objectMapper.readValue(bytes, GroupFullDto.class);

        //then
        assertNotNull(savedGroup);
        assertNotNull(savedGroup.getId());
        assertEquals(groupToSave.getName(), savedGroup.getName());
        assertEquals(groupToSave.getSubjectOfStudy(), savedGroup.getSubjectOfStudy());
        assertEquals(groupToSave.getStartDate(), savedGroup.getStartDate());
        assertEquals(groupToSave.getTrainingPeriod(), savedGroup.getTrainingPeriod());
    }

    @Test
    void create_whenNameIsTaken() throws Exception {
        //given
        GroupEntity groupToSave = generateGroup();
        groupRepository.save(groupToSave);
        GroupCreateDto groupToSave2 = generateGroupCreateDto();

        //when-then
        mockMvc.perform(post("/groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(groupToSave2)))
            .andExpect(status().isBadRequest())
            .andReturn();
    }

    @Test
    void update_happyPath() throws Exception {
        //given
        GroupEntity savedGroup = groupRepository.save(generateGroup());

        GroupUpdateDto groupToUpdate = generateGroupUpdateDto(savedGroup.getId(),
            savedGroup.getStartDate());

        TeacherCreateDto teacherCreateDto = generateTeacherCreateDto();
        TeacherFullDto savedTeacher = teacherService.create(teacherCreateDto);
        groupToUpdate.setTeacherId(savedTeacher.getId());

        //when
        MvcResult result = mockMvc.perform(put("/groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(groupToUpdate)))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        GroupFullDto updatedGroup = objectMapper.readValue(bytes, GroupFullDto.class);

        //then
        assertNotNull(updatedGroup);
        assertEquals(groupToUpdate.getId(), updatedGroup.getId());
        assertEquals(groupToUpdate.getName(), updatedGroup.getName());
        assertEquals(groupToUpdate.getSubjectOfStudy(), updatedGroup.getSubjectOfStudy());
        assertEquals(groupToUpdate.getStartDate(), updatedGroup.getStartDate());
        assertEquals(groupToUpdate.getTrainingPeriod(), updatedGroup.getTrainingPeriod());
        assertEquals(groupToUpdate.getTeacherId(), updatedGroup.getTeacher().getId());
    }

    @Test
    void update_whenNotFound() throws Exception {
        //given
        GroupEntity savedGroup = groupRepository.save(generateGroup());

        GroupUpdateDto groupToUpdate = generateGroupUpdateDto(savedGroup.getId(),
            savedGroup.getStartDate());
        Integer notExistingId = 10000;
        groupToUpdate.setId(notExistingId);

        //when-then
        mockMvc.perform(put("/groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(groupToUpdate)))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void update_whenTeacherNotFound() throws Exception {
        //given
        GroupEntity savedGroup = groupRepository.save(generateGroup());

        GroupUpdateDto groupToUpdate = generateGroupUpdateDto(savedGroup.getId(),
            savedGroup.getStartDate());

        //when-then
        mockMvc.perform(put("/groups")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(groupToUpdate)))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        GroupEntity savedGroup = groupRepository.save(generateGroup());

        //when
        mockMvc.perform(delete("/groups/{id}", savedGroup.getId()))
            .andExpect(status().isOk());

        //then
        mockMvc.perform(delete("/groups/{id}", savedGroup.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when-then
        mockMvc.perform(delete("/groups/{id}", notExistingId))
            .andExpect(status().isNotFound());
    }

    @Test
    void findByName_happyPath() throws Exception {
        //given
        GroupEntity savedGroup = groupRepository.save(generateGroup());

        //when
        MvcResult result = mockMvc.perform(get("/groups/name/{groupName}", savedGroup.getName()))
                .andExpect(status().isOk())
                .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        GroupFullDto foundGroup = objectMapper.readValue(bytes, GroupFullDto.class);

        //then
        assertNotNull(foundGroup);
        assertNotNull(foundGroup.getId());
    }
}
