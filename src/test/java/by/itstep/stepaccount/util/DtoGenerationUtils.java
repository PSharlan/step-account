package by.itstep.stepaccount.util;

import static by.itstep.stepaccount.entity.enums.Subject.HISTORY;
import static by.itstep.stepaccount.entity.enums.Subject.MATHEMATICS;
import static by.itstep.stepaccount.entity.enums.TrainingPeriod.FOR_YEARS;
import static by.itstep.stepaccount.entity.enums.TrainingPeriod.THREE_YEARS;
import by.itstep.stepaccount.dto.auth.UserLoginDto;
import by.itstep.stepaccount.dto.group.GroupCreateDto;
import by.itstep.stepaccount.dto.group.GroupUpdateDto;
import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentUpdateDto;
import by.itstep.stepaccount.dto.teacher.TeacherCreateDto;
import by.itstep.stepaccount.dto.teacher.TeacherUpdateDto;
import by.itstep.stepaccount.entity.UserEntity;

import java.time.Instant;

public class DtoGenerationUtils {

    public static StudentCreateDto generateStudentCreateDto(Integer groupId) {

        StudentCreateDto studentCreateDto = new StudentCreateDto();
        studentCreateDto.setName("Stephan");
        studentCreateDto.setLastName("Stephanovich");
        studentCreateDto.setEmail("steph@gmail.com");
        studentCreateDto.setPassword("fifty_fifty");
        studentCreateDto.setGroupId(groupId);
        studentCreateDto.setRating(3.3);

        return studentCreateDto;
    }

    public static StudentCreateDto generateStudentCreateDto() {

        StudentCreateDto studentCreateDto = new StudentCreateDto();
        studentCreateDto.setName("Stephan");
        studentCreateDto.setLastName("Stephanovich");
        studentCreateDto.setEmail("steph@gmail.com" + Math.random());
        studentCreateDto.setPassword("fifty_fifty");
        studentCreateDto.setGroupId(10000);
        studentCreateDto.setRating(1.5);

        return studentCreateDto;
    }

    public static StudentUpdateDto generateStudentUpdateDto(Integer studentId, Integer groupId) {

        StudentUpdateDto updateDto = new StudentUpdateDto();
        updateDto.setName("Charly");
        updateDto.setLastName("Axe");
        updateDto.setPassword("909099");
        updateDto.setId(studentId);
        updateDto.setGroupId(groupId);
        updateDto.setRating(5.5);

        return updateDto;
    }

    public static TeacherCreateDto generateTeacherCreateDto() {

        TeacherCreateDto teacherCreateDto = new TeacherCreateDto();
        teacherCreateDto.setName("Pavel");
        teacherCreateDto.setLastName("Sharlan");
        teacherCreateDto.setPassword("456789123");
        teacherCreateDto.setEmail(String.format("teacher@gmail.com%s", Math.random()));

        return teacherCreateDto;
    }

    public static TeacherUpdateDto generateTeacherUpdateDto(Integer teacherId) {

        TeacherUpdateDto teacherToUpdate = new TeacherUpdateDto();
        teacherToUpdate.setId(teacherId);
        teacherToUpdate.setName("Dmi");
        teacherToUpdate.setLastName("Axe");
        teacherToUpdate.setEmail("axe@gmail.com");
        teacherToUpdate.setPassword("54322");

        return teacherToUpdate;
    }

    public static GroupCreateDto generateGroupCreateDto() {

        GroupCreateDto createDto = new GroupCreateDto();
        createDto.setStartDate(Instant.now());
        createDto.setSubjectOfStudy(MATHEMATICS);
        createDto.setTrainingPeriod(FOR_YEARS);
        createDto.setName("J2019");

        return createDto;
    }

    public static GroupUpdateDto generateGroupUpdateDto(Integer groupId, Instant startDate) {

        GroupUpdateDto updateDto = new GroupUpdateDto();
        updateDto.setId(groupId);
        updateDto.setName("J2020");
        updateDto.setSubjectOfStudy(HISTORY);
        updateDto.setStartDate(startDate);
        updateDto.setTrainingPeriod(THREE_YEARS);
        updateDto.setTeacherId(10000);

        return updateDto;
    }

    public static UserLoginDto createUserLoginDto(final UserEntity userEntity) {
        UserLoginDto loginDto = new UserLoginDto();
        loginDto.setEmail(userEntity.getEmail());
        loginDto.setPassword(userEntity.getPassword());
        return loginDto;
    }

    public static UserLoginDto createUserLoginDtoWithBadCredentials() {
        UserLoginDto loginDto = new UserLoginDto();
        loginDto.setEmail("bad");
        loginDto.setPassword("bad");
        return loginDto;
    }
}
