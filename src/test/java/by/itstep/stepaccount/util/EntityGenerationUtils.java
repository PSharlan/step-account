package by.itstep.stepaccount.util;

import by.itstep.stepaccount.entity.AdminEntity;
import by.itstep.stepaccount.entity.GroupEntity;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.entity.enums.Subject;
import by.itstep.stepaccount.entity.enums.TrainingPeriod;
import by.itstep.stepaccount.entity.enums.UserRole;

import java.time.Instant;
import java.util.List;

public class EntityGenerationUtils {

    public static List<UserEntity> getEntitiesForRequest() {
        return List.of(
            UserEntity.builder()
                .name("Bobi")
                .lastName("Axe")
                .email("axe@gmail.com")
                .password("2344")
                .userRole(UserRole.STUDENT)
                .registrationDate(Instant.now())
                .build(),
            UserEntity.builder()
                .name("Bobi")
                .lastName("Axe")
                .email("ae@gmail.com")
                .password("2344")
                .userRole(UserRole.STUDENT)
                .registrationDate(Instant.now())
                .build(),
            UserEntity.builder()
                .name("Bobi")
                .lastName("Axe")
                .email("xe@gmail.com")
                .password("2344")
                .userRole(UserRole.STUDENT)
                .registrationDate(Instant.now())
                .build());
    }

    public static UserEntity generateUser() {

        return UserEntity.builder()
            .name("Bobi")
            .lastName("Axe")
            .email("axe@gmail.com")
            .password("2344")
            .userRole(UserRole.STUDENT)
            .registrationDate(Instant.now())
            .build();
    }

    public static UserEntity generateUser(UserRole role) {

        return UserEntity.builder()
            .name("Bobi")
            .userRole(role)
            .lastName("Axe22")
            .email("axe22@gmail.com" + Math.random())
            .password("2344")
            .registrationDate(Instant.now())
            .build();
    }

    public static AdminEntity generateAdmin() {

        return AdminEntity.builder()
            .login("new")
            .password("1233")
            .build();
    }

    public static GroupEntity generateGroup() {

        return GroupEntity.builder()
            .name("J2019")
            .subjectOfStudy(Subject.COMPUTER_SCIENCE)
            .startDate(Instant.now())
            .trainingPeriod(TrainingPeriod.ONE_YEARS)
            .build();
    }

    public static List<UserEntity> generateStudents() {
        return List.of(
            UserEntity.builder()
                .name("Viktor")
                .lastName("Viktor")
                .email("viktor@gmail.com")
                .password("5677")
                .userRole(UserRole.STUDENT)
                .registrationDate(Instant.now())
                .build(),
            UserEntity.builder()
                .name("Dmitri")
                .lastName("Dmitri")
                .email("dmitri@gmail.com")
                .password("2344")
                .userRole(UserRole.STUDENT)
                .registrationDate(Instant.now())
                .build(),
            UserEntity.builder()
                .name("Rita")
                .lastName("Rita")
                .email("rita@gmail.com")
                .password("7899")
                .userRole(UserRole.STUDENT)
                .registrationDate(Instant.now())
                .build());
    }

    public static List<UserEntity> generateTeachers() {
        return List.of(
            UserEntity.builder()
                .name("Viktor")
                .lastName("Viktor")
                .email("viktor@gmail.com")
                .password("5677")
                .userRole(UserRole.TEACHER)
                .registrationDate(Instant.now())
                .build(),
            UserEntity.builder()
                .name("Dmitri")
                .lastName("Dmitri")
                .email("dmitri@gmail.com")
                .password("2344")
                .userRole(UserRole.TEACHER)
                .registrationDate(Instant.now())
                .build(),
            UserEntity.builder()
                .name("Rita")
                .lastName("Rita")
                .email("rita@gmail.com")
                .password("7899")
                .userRole(UserRole.TEACHER)
                .registrationDate(Instant.now())
                .build());
    }

    public static List<AdminEntity> generateAdmins() {
        return List.of(
            AdminEntity.builder()
                .login("login1")
                .password("4566")
                .build(),
            AdminEntity.builder()
                .login("login2")
                .password("5677")
                .build(),
            AdminEntity.builder()
                .login("login3")
                .password("6788")
                .build());
    }

    public static List<GroupEntity> generateListGroup() {
        return List.of(
            GroupEntity.builder()
                .name("J2019")
                .subjectOfStudy(Subject.COMPUTER_SCIENCE)
                .startDate(Instant.now())
                .trainingPeriod(TrainingPeriod.TWO_YEARS)
                .build(),
            GroupEntity.builder()
                .name("J2021")
                .subjectOfStudy(Subject.GEOGRAPHY)
                .startDate(Instant.now())
                .trainingPeriod(TrainingPeriod.FOR_YEARS)
                .build(),
            GroupEntity.builder()
                .name("J2022")
                .subjectOfStudy(Subject.HISTORY)
                .startDate(Instant.now())
                .trainingPeriod(TrainingPeriod.THREE_YEARS)
                .build());
    }
}
