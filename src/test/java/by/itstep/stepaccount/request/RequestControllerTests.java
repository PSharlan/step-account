package by.itstep.stepaccount.request;

import static by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus.ACCEPTED;
import static by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus.PENDING;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateStudentCreateDto;
import static by.itstep.stepaccount.util.DtoGenerationUtils.generateTeacherCreateDto;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import by.itstep.stepaccount.dto.registrationRequest.RegistrationRequestFullDto;
import by.itstep.stepaccount.dto.student.StudentCreateDto;
import by.itstep.stepaccount.dto.student.StudentFullDto;
import by.itstep.stepaccount.dto.teacher.TeacherFullDto;
import by.itstep.stepaccount.entity.GroupEntity;
import by.itstep.stepaccount.entity.RegistrationRequestEntity;
import by.itstep.stepaccount.repository.GroupRepository;
import by.itstep.stepaccount.repository.RegistrationRequestRepository;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.StudentService;
import by.itstep.stepaccount.service.TeacherService;
import by.itstep.stepaccount.util.EntityGenerationUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
public class RequestControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RegistrationRequestRepository requestRepository;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void clearDatabase() {
        userRepository.deleteAll();
        groupRepository.deleteAll();
    }

    @Test
    void findAll_happyPath() throws Exception {
        //given
        List<TeacherFullDto> teachers = List.of(
            teacherService.create(generateTeacherCreateDto()),
            teacherService.create(generateTeacherCreateDto()),
            teacherService.create(generateTeacherCreateDto()));

        Pageable firstPageWithThreeElements = PageRequest.of(0, 3);

        //when
        mockMvc.perform(get("/registration/requests",firstPageWithThreeElements))
            .andExpect(status().isOk())
            .andReturn();
    }

    @Test
    void findAllByGroupId_happyPath() throws Exception {
        //given
        GroupEntity groupEntity = groupRepository.save(EntityGenerationUtils.generateGroup());
        List<StudentFullDto> savedStudentsWithGroupId = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            StudentCreateDto studentCreateDto = generateStudentCreateDto();
            studentCreateDto.setGroupId(groupEntity.getId());
            savedStudentsWithGroupId.add(studentService.create(studentCreateDto));
        }

        //when
        MvcResult result = mockMvc.perform(get("/registration/requests/group/{groupId}",groupEntity.getId()))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        List<RegistrationRequestFullDto> foundRequests = objectMapper.readValue(bytes, new TypeReference<>() {});

        //then
            assertEquals(savedStudentsWithGroupId.size(), foundRequests.size());
    }

    @Test
    void confirm_happyPath() throws Exception {
        //given
        TeacherFullDto savedTeacher = teacherService.create(generateTeacherCreateDto());

        RegistrationRequestEntity foundRequest = requestRepository.findByUserId(savedTeacher.getId());

        //when
        MvcResult result = mockMvc.perform(put("/registration/requests/{id}/confirm", foundRequest.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(foundRequest.getId())))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        RegistrationRequestFullDto confirmedRequest = objectMapper.readValue(bytes, RegistrationRequestFullDto.class);

        //then
        assertEquals(PENDING, foundRequest.getStatus());
        assertEquals(ACCEPTED, confirmedRequest.getStatus());
    }

    @Test
    void confirm_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when-then
        mockMvc.perform(put("/registration/requests/{id}/confirm", notExistingId)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound())
            .andReturn();
    }
}
