package by.itstep.stepaccount.request;

import static by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus.ACCEPTED;
import static by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus.PENDING;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static by.itstep.stepaccount.util.EntityGenerationUtils.getEntitiesForRequest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import by.itstep.stepaccount.dto.registrationRequest.RegistrationRequestFullDto;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.entity.enums.RegistrationRequestEntityStatus;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.RegistrationRequestService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;

import java.util.List;

@SpringBootTest
class RequestServiceTests {

    @Autowired
    private RegistrationRequestService requestService;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        userRepository.deleteAll();
    }

    @Test
    void createRequest_happyPath() {
        //given
        UserEntity entityForRequest = userRepository.save(generateUser());

        //when
        RegistrationRequestFullDto savedRequest = requestService.createRequest(entityForRequest);
        RegistrationRequestEntityStatus savedRequestStatus = savedRequest.getStatus();

        //then
        assertNotNull(savedRequest);
        assertNotNull(savedRequest.getUserId());
        assertEquals(PENDING, savedRequestStatus);
    }

    @Test
    void confirmRequest_happyPath() {
        //given
        UserEntity entityForRequest = userRepository.save(generateUser());

        //when
        RegistrationRequestFullDto savedRequest = requestService.createRequest(entityForRequest);

        RegistrationRequestFullDto confirmedRequestFullDto = requestService.confirmRequest(savedRequest.getId());

        //then
        assertEquals(savedRequest.getUserId(), confirmedRequestFullDto.getUserId());
        assertEquals(savedRequest.getUserName(), confirmedRequestFullDto.getUserName());
        assertEquals(savedRequest.getUserLastName(), confirmedRequestFullDto.getUserLastName());
        assertEquals(savedRequest.getUserEmail(), confirmedRequestFullDto.getUserEmail());
        assertEquals(PENDING, savedRequest.getStatus());
        assertEquals(ACCEPTED, confirmedRequestFullDto.getStatus());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<UserEntity> entitiesForRequest = getEntitiesForRequest();

        for (UserEntity userEntity : entitiesForRequest) {
            UserEntity savedUser = userRepository.save(userEntity);
            requestService.createRequest(savedUser);
        }

        //when
        List<RegistrationRequestFullDto> foundRequests = requestService.findAll(Pageable.unpaged()).getContent();

        //then
        assertEquals(foundRequests.size(), entitiesForRequest.size());
        foundRequests.forEach((request) -> assertNotNull(request.getUserId()));
    }
}
