package by.itstep.stepaccount.admin;

import static by.itstep.stepaccount.util.EntityGenerationUtils.generateAdmin;
import static org.junit.jupiter.api.Assertions.assertEquals;
import by.itstep.stepaccount.dto.admin.AdminFullDto;
import by.itstep.stepaccount.entity.AdminEntity;
import by.itstep.stepaccount.mapper.AdminMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AdminMapperTests {

    @Autowired
    private AdminMapper mapper;

    @Test
    void givenEntityToFullDto_whenMaps_thenCorrect() {
        //given
        AdminEntity adminEntity = generateAdmin();

        //when
        AdminFullDto adminFullDto = mapper.mapToFullDto(adminEntity);

        //then
        assertEquals(adminEntity.getLogin(), adminFullDto.getLogin());
        assertEquals(adminEntity.getId(), adminFullDto.getId());
    }
}
