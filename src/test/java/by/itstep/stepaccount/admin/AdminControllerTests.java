package by.itstep.stepaccount.admin;

import static by.itstep.stepaccount.util.EntityGenerationUtils.generateAdmin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import by.itstep.stepaccount.dto.admin.AdminFullDto;
import by.itstep.stepaccount.entity.AdminEntity;
import by.itstep.stepaccount.repository.AdminRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@SpringBootTest
@AutoConfigureMockMvc
public class AdminControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AdminRepository adminRepository;

    @BeforeEach
    void clearDatabase() {
        adminRepository.deleteAll();
    }

    @Test
    void findById_happyPath() throws Exception {
        //given
        AdminEntity savedAdmin = adminRepository.save(generateAdmin());

        //when
        MvcResult result = mockMvc.perform(get("/admins/{id}", savedAdmin.getId()))
            .andExpect(status().isOk())
            .andReturn();

        byte[] bytes = result.getResponse().getContentAsByteArray();
        AdminFullDto foundAdmin = objectMapper.readValue(bytes, AdminFullDto.class);

        //then
        Assertions.assertNotNull(foundAdmin);
        Assertions.assertNotNull(foundAdmin.getId());
        Assertions.assertEquals(savedAdmin.getId(), foundAdmin.getId());
    }

    @Test
    void findById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when-then
        mockMvc.perform(get("/admins/{id}", notExistingId))
            .andExpect(status().isNotFound())
            .andReturn();
    }

    @Test
    void deleteById_happyPath() throws Exception {
        //given
        AdminEntity savedAdmin = adminRepository.save(generateAdmin());

        //when
        mockMvc.perform(delete("/admins/{id}", savedAdmin.getId()))
            .andExpect(status().isOk());

        //then
        mockMvc.perform(delete("/admins/{id}", savedAdmin.getId()))
            .andExpect(status().isNotFound());
    }

    @Test
    void deleteById_whenNotFound() throws Exception {
        //given
        Integer notExistingId = 10000;

        //when-then
        mockMvc.perform(delete("/admins/{id}", notExistingId))
            .andExpect(status().isNotFound());
    }
}
