package by.itstep.stepaccount.admin;

import static by.itstep.stepaccount.util.EntityGenerationUtils.generateAdmin;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateAdmins;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import by.itstep.stepaccount.entity.AdminEntity;
import by.itstep.stepaccount.repository.AdminRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest
class AdminRepositoryTests {

    @Autowired
    private AdminRepository adminRepository;

    @BeforeEach
    void setUp() {
        adminRepository.deleteAll();
    }

    @Test
    void findByLogin_happyPath() {
        //given
        AdminEntity toSave = generateAdmin();
        AdminEntity saved = adminRepository.save(toSave);

        //when
        AdminEntity found = adminRepository.findByLogin(saved.getLogin());

        //then
        assertNotNull(found);
        assertEquals(saved.getId(), found.getId());
        assertEquals(saved.getLogin(), found.getLogin());
    }

    @Test
    void findById_happyPath() {
        //given
        AdminEntity toSave = generateAdmin();
        AdminEntity saved = adminRepository.save(toSave);

        //when
        Optional<AdminEntity> found = adminRepository.findById(saved.getId());

        //then
        assertTrue(found.isPresent());
        assertEquals(saved.getId(), found.get().getId());
        assertEquals(saved.getLogin(), found.get().getLogin());
    }

    @Test
    void findAll_happyPath() {
        //given
        List<AdminEntity> toSave = generateAdmins();

        for (AdminEntity admin : toSave) {
            adminRepository.save(admin);
        }

        //when
        List<AdminEntity> found = adminRepository.findAll();

        //then
        assertEquals(toSave.size(), found.size());
    }

    @Test
    void create_happyPath() {
        //given
        AdminEntity toSave = generateAdmin();

        //when
        AdminEntity saved = adminRepository.save(toSave);

        //then
        assertNotNull(saved);
        assertNotNull(saved.getId());
    }

    @Test
    void update_happyPath() {
        //given
        String newLogin = "updatedLogin";
        AdminEntity adminToSave = generateAdmin();

        AdminEntity savedAdmin = adminRepository.save(adminToSave);
        String login = savedAdmin.getLogin();
        savedAdmin.setLogin(newLogin);
        //when
        AdminEntity updatedAdmin = adminRepository.save(savedAdmin);

        //then
        assertNotEquals(login, updatedAdmin.getLogin());
        assertEquals(newLogin, updatedAdmin.getLogin());
    }

    @Test
    void deleteById_happyPath() {
        //given
        AdminEntity toSave = generateAdmin();
        AdminEntity saved = adminRepository.save(toSave);

        //when
        adminRepository.deleteById(saved.getId());

        //then
        assertTrue(adminRepository.findById(saved.getId()).isEmpty());
    }
}
