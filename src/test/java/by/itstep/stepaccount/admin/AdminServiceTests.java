package by.itstep.stepaccount.admin;

import static by.itstep.stepaccount.util.EntityGenerationUtils.generateAdmin;
import static by.itstep.stepaccount.util.EntityGenerationUtils.generateUser;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import by.itstep.stepaccount.dto.admin.AdminFullDto;
import by.itstep.stepaccount.entity.AdminEntity;
import by.itstep.stepaccount.entity.UserEntity;
import by.itstep.stepaccount.exception.UserCredentialsAreTakenException;
import by.itstep.stepaccount.repository.AdminRepository;
import by.itstep.stepaccount.repository.UserRepository;
import by.itstep.stepaccount.service.AdminService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AdminServiceTests {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private AdminService adminService;

    @Autowired
    private UserRepository userRepository;

    @BeforeEach
    void setUp() {
        adminRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    void findById_happyPath() {
        //given
        AdminEntity adminSaved = adminRepository.save(generateAdmin());

        //when
        AdminFullDto adminFound = adminService.findById(adminSaved.getId());

        //then
        assertNotNull(adminFound);
        assertEquals(adminSaved.getId(), adminFound.getId());
    }

    @Test
    void findById_whenNotFound() {
        //given
        Integer notExistingId = 1000;

        //when
        Exception exception = assertThrows(RuntimeException.class,
            () -> adminService.findById(notExistingId));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExistingId)));
    }

    @Test
    void deleteById_happyPath() {
        //given
        AdminEntity savedAdmin = adminRepository.save(generateAdmin());

        //when
        var savedAdminId = savedAdmin.getId();
        adminService.deleteById(savedAdminId);

        //then
        assertThrows(RuntimeException.class,
            () -> adminService.findById(savedAdminId));
    }

    @Test
    void deleteById_whenNotFound() {
        //given
        Integer notExisting = 1000;

        //when
        Exception exception = assertThrows(RuntimeException.class,
            () -> adminService.deleteById(notExisting));

        //then
        assertTrue(exception.getMessage().contains(String.valueOf(notExisting)));
    }

    @Test
    void checkIfEmailTaken_whenEmailIsTaken() {
        //given
        UserEntity userEntityToSave = generateUser();
        userRepository.save(userEntityToSave);

        UserEntity entityToCheckEmail = generateUser();

        //when - then
        Exception exception = assertThrows(UserCredentialsAreTakenException.class,
            () -> adminService.checkIfEmailTaken(entityToCheckEmail));

        //then
        assertTrue(exception.getMessage().contains(entityToCheckEmail.getEmail()));
    }
}
