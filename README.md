# Admin:  
### AdminFullDto:  
private Integer id;->(not null)  
private String login;->(not null)
***
# Group:  
### GroupCreateDto:  
private Subject subjectOfStudy;->(not null)  
private Instant startDate;->(not null)  
private TrainingPeriod trainingPeriod;->(not null)
### GroupFullDto:  
private Integer id;->(not null)  
private Subject subjectOfStudy;->(not null)  
private String name;->(not null)  
private Instant startDate;->(not null)  
private TrainingPeriod trainingPeriod;->(not null)  
private List &lt;StudentPreviewDto> students;  
private TeacherPreviewDto teacher;  
### GroupPreviewDto:  
private Integer id;->(not null)  
private Subject subjectOfStudy;->(not null)  
private String name;->(not null)  
private Instant startDate;->(not null)  
private TeacherPreviewDto teacher;  
### GroupUpdateDto:  
private Integer id;->(not null)  
private Subject subjectOfStudy;->(not null)  
private Instant startDate;->(not null)  
private TrainingPeriod trainingPeriod;->(not null)  
private Integer teacherId;
***
# RegistrationRequest:  
### RegistrationRequestFullDto:  
private Integer userId;->(not null)
***
# Student:  
### StudentCreateDto:  
private String name;->(not null)  
private String lastName;->(not null)  
private String email;->(not null)  
private String password;->(not null)  
private Integer groupId;  
private UserRole userRole;->(not null)  
private Instant registrationDate;
### StudentFullDto:  
private Integer id;->(not null)  
private String name;->(not null)  
private String lastName;->(not null)  
private String email;->(not null)  
private Instant registrationDate;->(not null)  
private Instant lastLoginDate;  
private GroupPreviewDto group;  
private UserRole userRole;->(not null)  
### StudentPreviewDto:  
private Integer id;->(not null)  
private String name;->(not null)  
private String lastName;->(not null)  
### StudentUpdateDto:  
private Integer id;->(not null)  
private String name;->(not null)  
private String lastName;->(not null)  
private String password;->(not null)  
private Integer groupId;  
private UserRole userRole;->(not null)
***
# Teacher:  
### TeacherCreateDto:  
private String name;->(not null)  
private String lastName;->(not null)  
private String email;->(not null)  
private String password;->(not null)  
private UserRole userRole;->(not null)  
### TeacherFullDto:  
private Integer id;->(not null)  
private String name;->(not null)  
private String lastName;->(not null)  
private String email;->(not null)  
private String password;->(not null)  
private Instant registrationDate;->(not null)  
private Instant lastLoginDate;  
private UserRole userRole;->(not null)  
private List &lt;GroupPreviewDto> teachingGroups;  
### TeacherPreviewDto:  
private Integer id;->(not null)  
private String name;->(not null)  
private String lastName;->(not null)  
### TeacherUpdateDto:  
private Integer id;->(not null)  
private String name;->(not null)  
private String lastName;->(not null)  
private String email;->(not null)  
private String password;->(not null)  
private UserRole userRole;->(not null)  
